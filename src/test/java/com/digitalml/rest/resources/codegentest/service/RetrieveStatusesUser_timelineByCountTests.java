package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesUser_timelineByCountInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesUser_timelineByCountReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveStatusesUser_timelineByCountTests {

	@Test
	public void testOperationRetrieveStatusesUser_timelineByCountBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveStatusesUser_timelineByCountInputParametersDTO inputs = new RetrieveStatusesUser_timelineByCountInputParametersDTO();
		inputs.setCount(null);
		inputs.setSince_id(null);
		inputs.setMax_id(null);
		inputs.setTrim_user(null);
		inputs.setExclude_replies(false);
		inputs.setContributor_details(false);
		inputs.setInclude_rts(false);
		RetrieveStatusesUser_timelineByCountReturnDTO returnValue = serviceDefaultImpl.retrieveStatusesUsertimelineByCount(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}