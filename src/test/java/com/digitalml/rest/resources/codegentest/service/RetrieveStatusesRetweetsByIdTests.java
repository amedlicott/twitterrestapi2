package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesRetweetsByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesRetweetsByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveStatusesRetweetsByIdTests {

	@Test
	public void testOperationRetrieveStatusesRetweetsByIdBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveStatusesRetweetsByIdInputParametersDTO inputs = new RetrieveStatusesRetweetsByIdInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setCount(null);
		inputs.setTrim_user(null);
		RetrieveStatusesRetweetsByIdReturnDTO returnValue = serviceDefaultImpl.retrieveStatusesRetweetsById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}