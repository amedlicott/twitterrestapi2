package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name1ReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateScreen_name1Tests {

	@Test
	public void testOperationCreateScreen_name1BasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		CreateScreen_name1InputParametersDTO inputs = new CreateScreen_name1InputParametersDTO();
		inputs.setScreen_name(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setUser_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setInclude_entities(null);
		inputs.setSkip_status(null);
		CreateScreen_name1ReturnDTO returnValue = serviceDefaultImpl.createScreenname1(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}