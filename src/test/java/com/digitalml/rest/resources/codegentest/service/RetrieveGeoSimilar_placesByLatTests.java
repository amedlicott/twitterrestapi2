package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSimilar_placesByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSimilar_placesByLatReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveGeoSimilar_placesByLatTests {

	@Test
	public void testOperationRetrieveGeoSimilar_placesByLatBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveGeoSimilar_placesByLatInputParametersDTO inputs = new RetrieveGeoSimilar_placesByLatInputParametersDTO();
		inputs.setLat(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.set_long(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setContained_within(null);
		inputs.setAttributeStreet_address(null);
		inputs.setCallback(null);
		RetrieveGeoSimilar_placesByLatReturnDTO returnValue = serviceDefaultImpl.retrieveGeoSimilarplacesByLat(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}