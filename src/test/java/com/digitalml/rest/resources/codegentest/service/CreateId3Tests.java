package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId3InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId3ReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateId3Tests {

	@Test
	public void testOperationCreateId3BasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		CreateId3InputParametersDTO inputs = new CreateId3InputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		CreateId3ReturnDTO returnValue = serviceDefaultImpl.createId3(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}