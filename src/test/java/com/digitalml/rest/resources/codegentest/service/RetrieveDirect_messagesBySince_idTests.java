package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesBySince_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesBySince_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveDirect_messagesBySince_idTests {

	@Test
	public void testOperationRetrieveDirect_messagesBySince_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveDirect_messagesBySince_idInputParametersDTO inputs = new RetrieveDirect_messagesBySince_idInputParametersDTO();
		inputs.setSince_id(null);
		inputs.setMax_id(null);
		inputs.setInclude_entities(null);
		inputs.setSkip_status(null);
		RetrieveDirect_messagesBySince_idReturnDTO returnValue = serviceDefaultImpl.retrieveDirectmessagesBySinceid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}