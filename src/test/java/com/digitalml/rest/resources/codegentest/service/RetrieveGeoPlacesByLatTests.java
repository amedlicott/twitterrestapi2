package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoPlacesByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoPlacesByLatReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveGeoPlacesByLatTests {

	@Test
	public void testOperationRetrieveGeoPlacesByLatBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveGeoPlacesByLatInputParametersDTO inputs = new RetrieveGeoPlacesByLatInputParametersDTO();
		inputs.setLat(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.set_long(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setToken(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setContained_within(null);
		inputs.setAttributeStreet_address(null);
		inputs.setCallback(null);
		RetrieveGeoPlacesByLatReturnDTO returnValue = serviceDefaultImpl.retrieveGeoPlacesByLat(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}