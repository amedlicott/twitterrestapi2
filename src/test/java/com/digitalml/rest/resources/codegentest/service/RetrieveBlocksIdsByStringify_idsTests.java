package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksIdsByStringify_idsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksIdsByStringify_idsReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveBlocksIdsByStringify_idsTests {

	@Test
	public void testOperationRetrieveBlocksIdsByStringify_idsBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveBlocksIdsByStringify_idsInputParametersDTO inputs = new RetrieveBlocksIdsByStringify_idsInputParametersDTO();
		inputs.setStringify_ids(null);
		inputs.setCursor(null);
		RetrieveBlocksIdsByStringify_idsReturnDTO returnValue = serviceDefaultImpl.retrieveBlocksIdsByStringifyids(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}