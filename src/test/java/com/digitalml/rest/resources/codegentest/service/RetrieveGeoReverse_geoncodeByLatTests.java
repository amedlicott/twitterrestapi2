package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoReverse_geoncodeByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoReverse_geoncodeByLatReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveGeoReverse_geoncodeByLatTests {

	@Test
	public void testOperationRetrieveGeoReverse_geoncodeByLatBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveGeoReverse_geoncodeByLatInputParametersDTO inputs = new RetrieveGeoReverse_geoncodeByLatInputParametersDTO();
		inputs.setLat(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.set_long(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setAccuracy(null);
		inputs.setGranularity(null);
		inputs.setMax_results(null);
		inputs.setCallback(null);
		RetrieveGeoReverse_geoncodeByLatReturnDTO returnValue = serviceDefaultImpl.retrieveGeoReversegeoncodeByLat(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}