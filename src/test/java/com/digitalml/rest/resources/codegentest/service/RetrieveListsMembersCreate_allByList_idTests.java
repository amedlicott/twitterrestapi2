package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersCreate_allByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersCreate_allByList_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveListsMembersCreate_allByList_idTests {

	@Test
	public void testOperationRetrieveListsMembersCreate_allByList_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveListsMembersCreate_allByList_idInputParametersDTO inputs = new RetrieveListsMembersCreate_allByList_idInputParametersDTO();
		inputs.setList_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setSlug(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setOwner_screen_name(null);
		inputs.setOwner_id(null);
		inputs.setUser_id(null);
		inputs.setScreen_name(null);
		RetrieveListsMembersCreate_allByList_idReturnDTO returnValue = serviceDefaultImpl.retrieveListsMembersCreateallByListid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}