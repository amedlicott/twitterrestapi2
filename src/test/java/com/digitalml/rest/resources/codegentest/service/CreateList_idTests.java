package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateList_idTests {

	@Test
	public void testOperationCreateList_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		CreateList_idInputParametersDTO inputs = new CreateList_idInputParametersDTO();
		inputs.setList_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setSlug(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setScreen_name(null);
		inputs.setOwner_screen_name(null);
		inputs.setOwner_id(null);
		CreateList_idReturnDTO returnValue = serviceDefaultImpl.createListid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}