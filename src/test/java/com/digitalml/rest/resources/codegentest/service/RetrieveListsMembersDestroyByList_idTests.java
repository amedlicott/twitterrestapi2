package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersDestroyByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersDestroyByList_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveListsMembersDestroyByList_idTests {

	@Test
	public void testOperationRetrieveListsMembersDestroyByList_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveListsMembersDestroyByList_idInputParametersDTO inputs = new RetrieveListsMembersDestroyByList_idInputParametersDTO();
		inputs.setList_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setSlug(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setOwner_screen_name(null);
		inputs.setUser_id(null);
		inputs.setScreen_name(null);
		inputs.setOwner_id(null);
		RetrieveListsMembersDestroyByList_idReturnDTO returnValue = serviceDefaultImpl.retrieveListsMembersDestroyByListid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}