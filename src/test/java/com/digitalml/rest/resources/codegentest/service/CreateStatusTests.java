package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateStatusInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateStatusReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateStatusTests {

	@Test
	public void testOperationCreateStatusBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		CreateStatusInputParametersDTO inputs = new CreateStatusInputParametersDTO();
		inputs.setStatus(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setIn_reply_to_status_id(null);
		inputs.setLat(null);
		inputs.set_long(null);
		inputs.setPlace_id(null);
		inputs.setDisplay_coordinates(null);
		inputs.setTrim_user(null);
		CreateStatusReturnDTO returnValue = serviceDefaultImpl.createStatus(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}