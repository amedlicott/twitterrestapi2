package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveApplicationRate_limit_statusByResourcesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveApplicationRate_limit_statusByResourcesReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveApplicationRate_limit_statusByResourcesTests {

	@Test
	public void testOperationRetrieveApplicationRate_limit_statusByResourcesBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveApplicationRate_limit_statusByResourcesInputParametersDTO inputs = new RetrieveApplicationRate_limit_statusByResourcesInputParametersDTO();
		inputs.setResources(new ArrayList&lt;String&gt;());
		RetrieveApplicationRate_limit_statusByResourcesReturnDTO returnValue = serviceDefaultImpl.retrieveApplicationRatelimitstatusByResources(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}