package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpTosInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpTosReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveHelpTosTests {

	@Test
	public void testOperationRetrieveHelpTosBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveHelpTosInputParametersDTO inputs = new RetrieveHelpTosInputParametersDTO();
		RetrieveHelpTosReturnDTO returnValue = serviceDefaultImpl.retrieveHelpTos(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}