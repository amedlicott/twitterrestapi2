package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersLookupByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersLookupByScreen_nameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveUsersLookupByScreen_nameTests {

	@Test
	public void testOperationRetrieveUsersLookupByScreen_nameBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveUsersLookupByScreen_nameInputParametersDTO inputs = new RetrieveUsersLookupByScreen_nameInputParametersDTO();
		inputs.setScreen_name(null);
		inputs.setUser_id(null);
		inputs.setInclude_entities(null);
		RetrieveUsersLookupByScreen_nameReturnDTO returnValue = serviceDefaultImpl.retrieveUsersLookupByScreenname(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}