package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersShowByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersShowByScreen_nameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveUsersShowByScreen_nameTests {

	@Test
	public void testOperationRetrieveUsersShowByScreen_nameBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveUsersShowByScreen_nameInputParametersDTO inputs = new RetrieveUsersShowByScreen_nameInputParametersDTO();
		inputs.setScreen_name(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setUser_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setInclude_entities(null);
		RetrieveUsersShowByScreen_nameReturnDTO returnValue = serviceDefaultImpl.retrieveUsersShowByScreenname(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}