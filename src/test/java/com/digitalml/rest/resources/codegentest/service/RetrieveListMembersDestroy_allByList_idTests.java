package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersDestroy_allByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersDestroy_allByList_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveListMembersDestroy_allByList_idTests {

	@Test
	public void testOperationRetrieveListMembersDestroy_allByList_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveListMembersDestroy_allByList_idInputParametersDTO inputs = new RetrieveListMembersDestroy_allByList_idInputParametersDTO();
		inputs.setList_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setSlug(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setUser_id(null);
		inputs.setScreen_name(null);
		inputs.setOwner_screen_name(null);
		inputs.setOwner_id(null);
		RetrieveListMembersDestroy_allByList_idReturnDTO returnValue = serviceDefaultImpl.retrieveListMembersDestroyallByListid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}