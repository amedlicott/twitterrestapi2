package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendsIdsByUser_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendsIdsByUser_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveFriendsIdsByUser_idTests {

	@Test
	public void testOperationRetrieveFriendsIdsByUser_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveFriendsIdsByUser_idInputParametersDTO inputs = new RetrieveFriendsIdsByUser_idInputParametersDTO();
		inputs.setUser_id(null);
		inputs.setScreen_name(null);
		inputs.setCursor(null);
		inputs.setStringify_ids(null);
		inputs.setCount(null);
		RetrieveFriendsIdsByUser_idReturnDTO returnValue = serviceDefaultImpl.retrieveFriendsIdsByUserid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}