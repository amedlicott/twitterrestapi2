package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesShowByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesShowByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveDirect_messagesShowByIdTests {

	@Test
	public void testOperationRetrieveDirect_messagesShowByIdBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveDirect_messagesShowByIdInputParametersDTO inputs = new RetrieveDirect_messagesShowByIdInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		RetrieveDirect_messagesShowByIdReturnDTO returnValue = serviceDefaultImpl.retrieveDirectmessagesShowById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}