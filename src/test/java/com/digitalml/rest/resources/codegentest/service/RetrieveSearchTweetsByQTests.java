package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSearchTweetsByQInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSearchTweetsByQReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveSearchTweetsByQTests {

	@Test
	public void testOperationRetrieveSearchTweetsByQBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveSearchTweetsByQInputParametersDTO inputs = new RetrieveSearchTweetsByQInputParametersDTO();
		inputs.setQ(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setGeocode(null);
		inputs.setLang(null);
		inputs.setLocale(null);
		inputs.setResult_type(null);
		inputs.setCount(null);
		inputs.setUntil(null);
		inputs.setSince_id(null);
		inputs.setMax_id(null);
		inputs.setInclude_entities(null);
		inputs.setCallback(null);
		RetrieveSearchTweetsByQReturnDTO returnValue = serviceDefaultImpl.retrieveSearchTweetsByQ(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}