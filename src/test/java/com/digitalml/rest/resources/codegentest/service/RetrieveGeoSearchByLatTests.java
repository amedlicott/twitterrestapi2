package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSearchByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSearchByLatReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveGeoSearchByLatTests {

	@Test
	public void testOperationRetrieveGeoSearchByLatBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveGeoSearchByLatInputParametersDTO inputs = new RetrieveGeoSearchByLatInputParametersDTO();
		inputs.setLat(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.set_long(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setQuery(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setIp(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setAccuracy(null);
		inputs.setGranularity(null);
		inputs.setContained_within(null);
		inputs.setAttributeStreet_address(null);
		inputs.setCallback(null);
		RetrieveGeoSearchByLatReturnDTO returnValue = serviceDefaultImpl.retrieveGeoSearchByLat(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}