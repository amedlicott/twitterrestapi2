package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class TwitterRESTAPI2Tests {

	@Test
	public void testResourceInitialisation() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationRetrieveListsSubscribersDestroyByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsSubscribersDestroyByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateList_id1NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createListid1(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveGeoReverse_geoncodeByLatNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveGeoReversegeoncodeByLat(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveSaved_searchesListNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveSavedsearchesList();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveHelpPrivacyNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveHelpPrivacy();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateScreen_nameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createScreenname(null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveHelpConfigurationNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveHelpConfiguration();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListMembersShowByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListMembersShowByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveStatusesShowByIdNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveStatusesShowById(org.apache.commons.lang3.StringUtils.EMPTY, null, org.apache.commons.lang3.StringUtils.EMPTY, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveGeoPlacesByLatNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveGeoPlacesByLat(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateScreen_name1NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createScreenname1(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveDirect_messagesSentBySince_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveDirectmessagesSentBySinceid(null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsSubscribersShowByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsSubscribersShowByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateDeviceNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createDevice(org.apache.commons.lang3.StringUtils.EMPTY, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveStatusesOembedByIdNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveStatusesOembedById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveGeoIdByPlace_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveGeoIdByPlaceid(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveUsersShowByScreen_nameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveUsersShowByScreenname(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateScreen_name2NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createScreenname2(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveDirect_messagesBySince_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveDirectmessagesBySinceid(null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveSaved_searchesShowByIdNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveSavedsearchesShowById(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsShowByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsShowByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateScreen_name3NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createScreenname3(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateSkip_statusNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createSkipstatus(null, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateIdNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createId(org.apache.commons.lang3.StringUtils.EMPTY, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveHelpLanguagesNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveHelpLanguages();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveUsersContributorsByScreen_nameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveUsersContributorsByScreenname(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveTrendsAvailableNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveTrendsAvailable();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveBlocksIdsByStringify_idsNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveBlocksIdsByStringifyids(null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveGeoSimilar_placesByLatNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveGeoSimilarplacesByLat(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveFriendshipsOutgoingByCursorNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveFriendshipsOutgoingByCursor(null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveApplicationRate_limit_statusByResourcesNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveApplicationRatelimitstatusByResources(new ArrayList&lt;String&gt;());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsSubscribersByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsSubscribersByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListMembersDestroy_allByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListMembersDestroyallByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsMembersCreate_allByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsMembersCreateallByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveFriendshipsIncomingByCursorNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveFriendshipsIncomingByCursor(null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveAccountSettingsNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveAccountSettings();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateTrend_location_woeidNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createTrendlocationwoeid(null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveTrendsClosestByLatNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveTrendsClosestByLat(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveUsersContributeesByScreen_nameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveUsersContributeesByScreenname(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsListByScreen_nameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsListByScreenname(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateTileNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createTile(null, null, null, null, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateNameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createName(null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveHelpTosNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveHelpTos();
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateQueryNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createQuery(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveStatusesRetweetsByIdNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveStatusesRetweetsById(org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateId1NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createId1(org.apache.commons.lang3.StringUtils.EMPTY, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateId2NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createId2(org.apache.commons.lang3.StringUtils.EMPTY, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveBlocksListByInclude_entitiesNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveBlocksListByIncludeentities(null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateStatusNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createStatus(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveFriendshipsShowBySource_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveFriendshipsShowBySourceid(null, null, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsStatusesByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsStatusesByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListMembersByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListMembersByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveFollowersIdsByUser_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveFollowersIdsByUserid(null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateProfile_background_colorNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createProfilebackgroundcolor(null, null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveStatusesUser_timelineByCountNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveStatusesUsertimelineByCount(null, null, null, null, false, false, false);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateScreen_name4NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createScreenname4(null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveUsersLookupByScreen_nameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveUsersLookupByScreenname(null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveTrendsPlaceByIdNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveTrendsPlaceById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateScreen_name5NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createScreenname5(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveUsersSearchByQNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveUsersSearchByQ(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveStatusesHome_timelineBySince_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveStatusesHometimelineBySinceid(null, null, null, false, false);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveSearchTweetsByQNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveSearchTweetsByQ(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateList_id2NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createListid2(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateUser_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createUserid(org.apache.commons.lang3.StringUtils.EMPTY, null, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveDirect_messagesShowByIdNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveDirectmessagesShowById(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveStatusesMentions_timelineByCountNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveStatusesMentionstimelineByCount(null, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsMembershipsByUser_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsMembershipsByUserid(null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateList_id3NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createListid3(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveFriendsIdsByUser_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveFriendsIdsByUserid(null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateName1NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createName1(null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsMembersDestroyByList_idNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsMembersDestroyByListid(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateId3NoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.createId3(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveListsSubscriptionsByScreen_nameNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveListsSubscriptionsByScreenname(null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationRetrieveGeoSearchByLatNoSecurity() {
		TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		resource.setSecurityContext(null);

		Response response = resource.retrieveGeoSearchByLat(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}