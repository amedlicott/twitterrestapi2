package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesHome_timelineBySince_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesHome_timelineBySince_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveStatusesHome_timelineBySince_idTests {

	@Test
	public void testOperationRetrieveStatusesHome_timelineBySince_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveStatusesHome_timelineBySince_idInputParametersDTO inputs = new RetrieveStatusesHome_timelineBySince_idInputParametersDTO();
		inputs.setSince_id(null);
		inputs.setMax_id(null);
		inputs.setTrim_user(null);
		inputs.setExclude_replies(false);
		inputs.setContributor_details(false);
		RetrieveStatusesHome_timelineBySince_idReturnDTO returnValue = serviceDefaultImpl.retrieveStatusesHometimelineBySinceid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}