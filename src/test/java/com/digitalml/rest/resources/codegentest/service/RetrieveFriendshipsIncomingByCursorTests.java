package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsIncomingByCursorInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsIncomingByCursorReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveFriendshipsIncomingByCursorTests {

	@Test
	public void testOperationRetrieveFriendshipsIncomingByCursorBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveFriendshipsIncomingByCursorInputParametersDTO inputs = new RetrieveFriendshipsIncomingByCursorInputParametersDTO();
		inputs.setCursor(null);
		inputs.setStringify_ids(null);
		RetrieveFriendshipsIncomingByCursorReturnDTO returnValue = serviceDefaultImpl.retrieveFriendshipsIncomingByCursor(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}