package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id1ReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateList_id1Tests {

	@Test
	public void testOperationCreateList_id1BasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		CreateList_id1InputParametersDTO inputs = new CreateList_id1InputParametersDTO();
		inputs.setList_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setSlug(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setOwner_screen_name(null);
		inputs.setOwner_id(null);
		inputs.setName(null);
		inputs.setMode(null);
		inputs.setDescription(null);
		CreateList_id1ReturnDTO returnValue = serviceDefaultImpl.createListid1(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}