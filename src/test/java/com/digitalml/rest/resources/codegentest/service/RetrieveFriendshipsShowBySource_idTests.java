package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsShowBySource_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsShowBySource_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveFriendshipsShowBySource_idTests {

	@Test
	public void testOperationRetrieveFriendshipsShowBySource_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveFriendshipsShowBySource_idInputParametersDTO inputs = new RetrieveFriendshipsShowBySource_idInputParametersDTO();
		inputs.setSource_id(null);
		inputs.setSource_screen_name(null);
		inputs.setTarget_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setTarget_screen_name(org.apache.commons.lang3.StringUtils.EMPTY);
		RetrieveFriendshipsShowBySource_idReturnDTO returnValue = serviceDefaultImpl.retrieveFriendshipsShowBySourceid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}