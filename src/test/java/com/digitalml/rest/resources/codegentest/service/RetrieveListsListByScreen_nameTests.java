package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsListByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsListByScreen_nameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveListsListByScreen_nameTests {

	@Test
	public void testOperationRetrieveListsListByScreen_nameBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveListsListByScreen_nameInputParametersDTO inputs = new RetrieveListsListByScreen_nameInputParametersDTO();
		inputs.setScreen_name(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setUser_id(org.apache.commons.lang3.StringUtils.EMPTY);
		RetrieveListsListByScreen_nameReturnDTO returnValue = serviceDefaultImpl.retrieveListsListByScreenname(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}