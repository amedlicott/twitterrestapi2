package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksListByInclude_entitiesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksListByInclude_entitiesReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveBlocksListByInclude_entitiesTests {

	@Test
	public void testOperationRetrieveBlocksListByInclude_entitiesBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveBlocksListByInclude_entitiesInputParametersDTO inputs = new RetrieveBlocksListByInclude_entitiesInputParametersDTO();
		inputs.setInclude_entities(null);
		inputs.setSkip_status(null);
		inputs.setCursor(null);
		RetrieveBlocksListByInclude_entitiesReturnDTO returnValue = serviceDefaultImpl.retrieveBlocksListByIncludeentities(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}