package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsStatusesByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsStatusesByList_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveListsStatusesByList_idTests {

	@Test
	public void testOperationRetrieveListsStatusesByList_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveListsStatusesByList_idInputParametersDTO inputs = new RetrieveListsStatusesByList_idInputParametersDTO();
		inputs.setList_id(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setSlug(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setOwner_screen_name(null);
		inputs.setOwner_id(null);
		inputs.setSince_id(null);
		inputs.setMax_id(null);
		inputs.setCount(null);
		inputs.setInclude_entities(null);
		inputs.setInclude_rts(null);
		RetrieveListsStatusesByList_idReturnDTO returnValue = serviceDefaultImpl.retrieveListsStatusesByListid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}