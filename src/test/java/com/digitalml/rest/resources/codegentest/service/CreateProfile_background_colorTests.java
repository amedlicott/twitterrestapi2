package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateProfile_background_colorInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateProfile_background_colorReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateProfile_background_colorTests {

	@Test
	public void testOperationCreateProfile_background_colorBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		CreateProfile_background_colorInputParametersDTO inputs = new CreateProfile_background_colorInputParametersDTO();
		inputs.setProfile_background_color(null);
		inputs.setProfile_link_color(null);
		inputs.setProfile_sidebar_border_color(null);
		inputs.setProfile_sidebar_fill_color(null);
		inputs.setProfile_text_color(null);
		inputs.setInclude_entities(null);
		inputs.setSkip_status(null);
		CreateProfile_background_colorReturnDTO returnValue = serviceDefaultImpl.createProfilebackgroundcolor(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}