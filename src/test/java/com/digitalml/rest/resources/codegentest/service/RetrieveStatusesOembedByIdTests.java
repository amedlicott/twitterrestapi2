package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesOembedByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesOembedByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveStatusesOembedByIdTests {

	@Test
	public void testOperationRetrieveStatusesOembedByIdBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveStatusesOembedByIdInputParametersDTO inputs = new RetrieveStatusesOembedByIdInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setUrl(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setMaxwidth(null);
		inputs.setHide_media(null);
		inputs.setHide_thread(null);
		inputs.setAlign(null);
		inputs.setRelated(null);
		inputs.setLang(null);
		RetrieveStatusesOembedByIdReturnDTO returnValue = serviceDefaultImpl.retrieveStatusesOembedById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}