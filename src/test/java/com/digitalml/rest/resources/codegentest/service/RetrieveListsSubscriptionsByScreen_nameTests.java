package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscriptionsByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscriptionsByScreen_nameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveListsSubscriptionsByScreen_nameTests {

	@Test
	public void testOperationRetrieveListsSubscriptionsByScreen_nameBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveListsSubscriptionsByScreen_nameInputParametersDTO inputs = new RetrieveListsSubscriptionsByScreen_nameInputParametersDTO();
		inputs.setScreen_name(null);
		inputs.setUser_id(null);
		inputs.setCount(null);
		inputs.setCursor(null);
		RetrieveListsSubscriptionsByScreen_nameReturnDTO returnValue = serviceDefaultImpl.retrieveListsSubscriptionsByScreenname(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}