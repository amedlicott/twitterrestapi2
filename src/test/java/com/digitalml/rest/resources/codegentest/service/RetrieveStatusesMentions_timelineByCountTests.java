package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesMentions_timelineByCountInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesMentions_timelineByCountReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveStatusesMentions_timelineByCountTests {

	@Test
	public void testOperationRetrieveStatusesMentions_timelineByCountBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveStatusesMentions_timelineByCountInputParametersDTO inputs = new RetrieveStatusesMentions_timelineByCountInputParametersDTO();
		inputs.setCount(null);
		inputs.setSince_id(null);
		inputs.setMax_id(null);
		inputs.setTrim_user(null);
		inputs.setContributor_details(null);
		inputs.setInclude_entities(null);
		RetrieveStatusesMentions_timelineByCountReturnDTO returnValue = serviceDefaultImpl.retrieveStatusesMentionstimelineByCount(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}