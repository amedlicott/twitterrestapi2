package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoIdByPlace_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoIdByPlace_idReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveGeoIdByPlace_idTests {

	@Test
	public void testOperationRetrieveGeoIdByPlace_idBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		RetrieveGeoIdByPlace_idInputParametersDTO inputs = new RetrieveGeoIdByPlace_idInputParametersDTO();
		inputs.setPlace_id(org.apache.commons.lang3.StringUtils.EMPTY);
		RetrieveGeoIdByPlace_idReturnDTO returnValue = serviceDefaultImpl.retrieveGeoIdByPlaceid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}