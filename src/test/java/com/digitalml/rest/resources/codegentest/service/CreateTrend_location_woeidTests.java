package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTrend_location_woeidInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTrend_location_woeidReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateTrend_location_woeidTests {

	@Test
	public void testOperationCreateTrend_location_woeidBasicMapping()  {
		TwitterRESTAPI2ServiceDefaultImpl serviceDefaultImpl = new TwitterRESTAPI2ServiceDefaultImpl();
		CreateTrend_location_woeidInputParametersDTO inputs = new CreateTrend_location_woeidInputParametersDTO();
		inputs.setTrend_location_woeid(null);
		inputs.setSleep_time_enabled(null);
		inputs.setStart_sleep_time(null);
		inputs.setEnd_sleep_time(null);
		inputs.setTime_zone(null);
		inputs.setLang(null);
		CreateTrend_location_woeidReturnDTO returnValue = serviceDefaultImpl.createTrendlocationwoeid(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}