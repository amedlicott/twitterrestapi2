package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Lists:
{
  "type": "object",
  "properties": {
    "created_at": {
      "type": "string"
    },
    "slug": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "mode": {
      "type": "string"
    },
    "following": {
      "type": "boolean"
    },
    "user": {
      "$ref": "Users"
    },
    "member_count": {
      "type": "integer",
      "format": "int32"
    },
    "id_str": {
      "type": "string"
    },
    "subscriber_count": {
      "type": "integer",
      "format": "int32"
    },
    "id": {
      "type": "integer",
      "format": "int32"
    },
    "uri": {
      "type": "string"
    }
  }
}
*/

public class Lists {

	@Size(max=1)
	private String created_at;

	@Size(max=1)
	private String slug;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String description;

	@Size(max=1)
	private String mode;

	@Size(max=1)
	private boolean following;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Users user;

	@Size(max=1)
	private Integer member_count;

	@Size(max=1)
	private String id_str;

	@Size(max=1)
	private Integer subscriber_count;

	@Size(max=1)
	private Integer id;

	@Size(max=1)
	private String uri;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    created_at = null;
	    slug = null;
	    name = null;
	    description = null;
	    mode = null;
	    
	    users = new com.digitalml.rest.resources.codegentest.Users();
	    
	    id_str = null;
	    
	    
	    uri = null;
	}
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getSlug() {
		return slug;
	}
	
	public void setSlug(String slug) {
		this.slug = slug;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMode() {
		return mode;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	public boolean getFollowing() {
		return following;
	}
	
	public void setFollowing(boolean following) {
		this.following = following;
	}
	public com.digitalml.rest.resources.codegentest.Users getUser() {
		return user;
	}
	
	public void setUser(com.digitalml.rest.resources.codegentest.Users user) {
		this.user = user;
	}
	public Integer getMember_count() {
		return member_count;
	}
	
	public void setMember_count(Integer member_count) {
		this.member_count = member_count;
	}
	public String getId_str() {
		return id_str;
	}
	
	public void setId_str(String id_str) {
		this.id_str = id_str;
	}
	public Integer getSubscriber_count() {
		return subscriber_count;
	}
	
	public void setSubscriber_count(Integer subscriber_count) {
		this.subscriber_count = subscriber_count;
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
}