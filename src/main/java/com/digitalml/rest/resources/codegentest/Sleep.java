package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Sleep:
{
  "type": "object",
  "properties": {
    "end_time": {
      "type": "string"
    },
    "enabled": {
      "type": "boolean"
    },
    "start_time": {
      "type": "string"
    }
  }
}
*/

public class Sleep {

	@Size(max=1)
	private String end_time;

	@Size(max=1)
	private boolean enabled;

	@Size(max=1)
	private String start_time;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    end_time = null;
	    
	    start_time = null;
	}
	public String getEnd_time() {
		return end_time;
	}
	
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	public boolean getEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getStart_time() {
		return start_time;
	}
	
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
}