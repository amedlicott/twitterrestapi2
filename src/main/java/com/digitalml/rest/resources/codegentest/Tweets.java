package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Tweets:
{
  "type": "object",
  "properties": {
    "contributors": {
      "type": "array",
      "items": {
        "$ref": "Contributors"
      }
    },
    "coordinates": {
      "$ref": "Coordinates"
    },
    "created_at": {
      "type": "string"
    },
    "current_user_retweet": {
      "type": "object"
    },
    "entities": {
      "$ref": "Entities"
    },
    "favorite_count": {
      "type": "integer",
      "format": "int32"
    },
    "favorited": {
      "type": "boolean"
    },
    "filter_level": {
      "type": "string"
    },
    "id": {
      "type": "integer",
      "format": "int32"
    },
    "id_str": {
      "type": "string"
    },
    "in_reply_to_screen_name": {
      "type": "string"
    },
    "in_reply_to_status_id": {
      "type": "integer",
      "format": "int32"
    },
    "in_reply_to_status_id_str": {
      "type": "string"
    },
    "in_reply_to_user_id": {
      "type": "integer",
      "format": "int32"
    },
    "in_reply_to_user_id_str": {
      "type": "string"
    },
    "lang": {
      "type": "string"
    },
    "place": {
      "$ref": "Places"
    },
    "possibly_sensitive": {
      "type": "boolean"
    },
    "quoted_status_id": {
      "type": "integer",
      "format": "int32"
    },
    "quoted_status_id_str": {
      "type": "string"
    },
    "quoted_status": {
      "type": "object"
    },
    "scopes": {
      "type": "string"
    },
    "retweet_count": {
      "type": "integer",
      "format": "int32"
    },
    "retweeted": {
      "type": "boolean"
    },
    "retweeted_status": {
      "type": "object"
    },
    "source": {
      "type": "string"
    },
    "text": {
      "type": "string"
    },
    "truncated": {
      "type": "string"
    },
    "user": {
      "$ref": "Users"
    },
    "withheld_copyright": {
      "type": "boolean"
    },
    "withheld_countries": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "withheld_scope": {
      "type": "string"
    }
  }
}
*/

public class Tweets {

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Contributors> contributors;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Coordinates coordinates;

	@Size(max=1)
	private String created_at;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Current_user_retweet current_user_retweet;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Entities entities;

	@Size(max=1)
	private Integer favorite_count;

	@Size(max=1)
	private boolean favorited;

	@Size(max=1)
	private String filter_level;

	@Size(max=1)
	private Integer id;

	@Size(max=1)
	private String id_str;

	@Size(max=1)
	private String in_reply_to_screen_name;

	@Size(max=1)
	private Integer in_reply_to_status_id;

	@Size(max=1)
	private String in_reply_to_status_id_str;

	@Size(max=1)
	private Integer in_reply_to_user_id;

	@Size(max=1)
	private String in_reply_to_user_id_str;

	@Size(max=1)
	private String lang;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Places place;

	@Size(max=1)
	private boolean possibly_sensitive;

	@Size(max=1)
	private Integer quoted_status_id;

	@Size(max=1)
	private String quoted_status_id_str;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Quoted_status quoted_status;

	@Size(max=1)
	private String scopes;

	@Size(max=1)
	private Integer retweet_count;

	@Size(max=1)
	private boolean retweeted;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Retweeted_status retweeted_status;

	@Size(max=1)
	private String source;

	@Size(max=1)
	private String text;

	@Size(max=1)
	private String truncated;

	@Size(max=1)
	private Users user;

	@Size(max=1)
	private boolean withheld_copyright;

	@Size(max=1)
	private List<String> withheld_countries;

	@Size(max=1)
	private String withheld_scope;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    contributors = new ArrayList<com.digitalml.rest.resources.codegentest.Contributors>();
	    coordinates = new com.digitalml.rest.resources.codegentest.Coordinates();
	    created_at = null;
	    current_user_retweet = null;
	    entities = new com.digitalml.rest.resources.codegentest.Entities();
	    
	    
	    filter_level = null;
	    
	    id_str = null;
	    in_reply_to_screen_name = null;
	    
	    in_reply_to_status_id_str = null;
	    
	    in_reply_to_user_id_str = null;
	    lang = null;
	    places = new com.digitalml.rest.resources.codegentest.Places();
	    
	    
	    quoted_status_id_str = null;
	    quoted_status = null;
	    scopes = null;
	    
	    
	    retweeted_status = null;
	    source = null;
	    text = null;
	    truncated = null;
	    user = null;
	    
	    withheld_countries = new ArrayList<String>();
	    withheld_scope = null;
	}
	public List<com.digitalml.rest.resources.codegentest.Contributors> getContributors() {
		return contributors;
	}
	
	public void setContributors(List<com.digitalml.rest.resources.codegentest.Contributors> contributors) {
		this.contributors = contributors;
	}
	public com.digitalml.rest.resources.codegentest.Coordinates getCoordinates() {
		return coordinates;
	}
	
	public void setCoordinates(com.digitalml.rest.resources.codegentest.Coordinates coordinates) {
		this.coordinates = coordinates;
	}
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public com.digitalml.rest.resources.codegentest.Current_user_retweet getCurrent_user_retweet() {
		return current_user_retweet;
	}
	
	public void setCurrent_user_retweet(com.digitalml.rest.resources.codegentest.Current_user_retweet current_user_retweet) {
		this.current_user_retweet = current_user_retweet;
	}
	public com.digitalml.rest.resources.codegentest.Entities getEntities() {
		return entities;
	}
	
	public void setEntities(com.digitalml.rest.resources.codegentest.Entities entities) {
		this.entities = entities;
	}
	public Integer getFavorite_count() {
		return favorite_count;
	}
	
	public void setFavorite_count(Integer favorite_count) {
		this.favorite_count = favorite_count;
	}
	public boolean getFavorited() {
		return favorited;
	}
	
	public void setFavorited(boolean favorited) {
		this.favorited = favorited;
	}
	public String getFilter_level() {
		return filter_level;
	}
	
	public void setFilter_level(String filter_level) {
		this.filter_level = filter_level;
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getId_str() {
		return id_str;
	}
	
	public void setId_str(String id_str) {
		this.id_str = id_str;
	}
	public String getIn_reply_to_screen_name() {
		return in_reply_to_screen_name;
	}
	
	public void setIn_reply_to_screen_name(String in_reply_to_screen_name) {
		this.in_reply_to_screen_name = in_reply_to_screen_name;
	}
	public Integer getIn_reply_to_status_id() {
		return in_reply_to_status_id;
	}
	
	public void setIn_reply_to_status_id(Integer in_reply_to_status_id) {
		this.in_reply_to_status_id = in_reply_to_status_id;
	}
	public String getIn_reply_to_status_id_str() {
		return in_reply_to_status_id_str;
	}
	
	public void setIn_reply_to_status_id_str(String in_reply_to_status_id_str) {
		this.in_reply_to_status_id_str = in_reply_to_status_id_str;
	}
	public Integer getIn_reply_to_user_id() {
		return in_reply_to_user_id;
	}
	
	public void setIn_reply_to_user_id(Integer in_reply_to_user_id) {
		this.in_reply_to_user_id = in_reply_to_user_id;
	}
	public String getIn_reply_to_user_id_str() {
		return in_reply_to_user_id_str;
	}
	
	public void setIn_reply_to_user_id_str(String in_reply_to_user_id_str) {
		this.in_reply_to_user_id_str = in_reply_to_user_id_str;
	}
	public String getLang() {
		return lang;
	}
	
	public void setLang(String lang) {
		this.lang = lang;
	}
	public com.digitalml.rest.resources.codegentest.Places getPlace() {
		return place;
	}
	
	public void setPlace(com.digitalml.rest.resources.codegentest.Places place) {
		this.place = place;
	}
	public boolean getPossibly_sensitive() {
		return possibly_sensitive;
	}
	
	public void setPossibly_sensitive(boolean possibly_sensitive) {
		this.possibly_sensitive = possibly_sensitive;
	}
	public Integer getQuoted_status_id() {
		return quoted_status_id;
	}
	
	public void setQuoted_status_id(Integer quoted_status_id) {
		this.quoted_status_id = quoted_status_id;
	}
	public String getQuoted_status_id_str() {
		return quoted_status_id_str;
	}
	
	public void setQuoted_status_id_str(String quoted_status_id_str) {
		this.quoted_status_id_str = quoted_status_id_str;
	}
	public com.digitalml.rest.resources.codegentest.Quoted_status getQuoted_status() {
		return quoted_status;
	}
	
	public void setQuoted_status(com.digitalml.rest.resources.codegentest.Quoted_status quoted_status) {
		this.quoted_status = quoted_status;
	}
	public String getScopes() {
		return scopes;
	}
	
	public void setScopes(String scopes) {
		this.scopes = scopes;
	}
	public Integer getRetweet_count() {
		return retweet_count;
	}
	
	public void setRetweet_count(Integer retweet_count) {
		this.retweet_count = retweet_count;
	}
	public boolean getRetweeted() {
		return retweeted;
	}
	
	public void setRetweeted(boolean retweeted) {
		this.retweeted = retweeted;
	}
	public com.digitalml.rest.resources.codegentest.Retweeted_status getRetweeted_status() {
		return retweeted_status;
	}
	
	public void setRetweeted_status(com.digitalml.rest.resources.codegentest.Retweeted_status retweeted_status) {
		this.retweeted_status = retweeted_status;
	}
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	public String getTruncated() {
		return truncated;
	}
	
	public void setTruncated(String truncated) {
		this.truncated = truncated;
	}
	public Users getUser() {
		return user;
	}
	
	public void setUser(Users user) {
		this.user = user;
	}
	public boolean getWithheld_copyright() {
		return withheld_copyright;
	}
	
	public void setWithheld_copyright(boolean withheld_copyright) {
		this.withheld_copyright = withheld_copyright;
	}
	public List<String> getWithheld_countries() {
		return withheld_countries;
	}
	
	public void setWithheld_countries(List<String> withheld_countries) {
		this.withheld_countries = withheld_countries;
	}
	public String getWithheld_scope() {
		return withheld_scope;
	}
	
	public void setWithheld_scope(String withheld_scope) {
		this.withheld_scope = withheld_scope;
	}
}