package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Help_Tos:
{
  "type": "object",
  "properties": {
    "Tos": {
      "type": "string"
    }
  }
}
*/

public class Help_Tos {

	@Size(max=1)
	private String tos;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    tos = null;
	}
	public String getTos() {
		return tos;
	}
	
	public void setTos(String tos) {
		this.tos = tos;
	}
}