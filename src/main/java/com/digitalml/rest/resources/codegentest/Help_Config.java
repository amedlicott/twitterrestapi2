package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Help_Config:
{
  "type": "object",
  "properties": {
    "dm_text_character_limit": {
      "type": "integer",
      "format": "int32"
    },
    "characters_reserved_per_media": {
      "type": "integer",
      "format": "int32"
    },
    "max_media_per_upload": {
      "type": "integer",
      "format": "int32"
    },
    "non_username_paths": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "photo_size_limit": {
      "type": "integer",
      "format": "int32"
    },
    "photo_sizes": {
      "$ref": "Sizes"
    }
  }
}
*/

public class Help_Config {

	@Size(max=1)
	private Integer dm_text_character_limit;

	@Size(max=1)
	private Integer characters_reserved_per_media;

	@Size(max=1)
	private Integer max_media_per_upload;

	@Size(max=1)
	private List<String> non_username_paths;

	@Size(max=1)
	private Integer photo_size_limit;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Sizes photo_sizes;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    
	    
	    non_username_paths = new ArrayList<String>();
	    
	    sizes = new com.digitalml.rest.resources.codegentest.Sizes();
	}
	public Integer getDm_text_character_limit() {
		return dm_text_character_limit;
	}
	
	public void setDm_text_character_limit(Integer dm_text_character_limit) {
		this.dm_text_character_limit = dm_text_character_limit;
	}
	public Integer getCharacters_reserved_per_media() {
		return characters_reserved_per_media;
	}
	
	public void setCharacters_reserved_per_media(Integer characters_reserved_per_media) {
		this.characters_reserved_per_media = characters_reserved_per_media;
	}
	public Integer getMax_media_per_upload() {
		return max_media_per_upload;
	}
	
	public void setMax_media_per_upload(Integer max_media_per_upload) {
		this.max_media_per_upload = max_media_per_upload;
	}
	public List<String> getNon_username_paths() {
		return non_username_paths;
	}
	
	public void setNon_username_paths(List<String> non_username_paths) {
		this.non_username_paths = non_username_paths;
	}
	public Integer getPhoto_size_limit() {
		return photo_size_limit;
	}
	
	public void setPhoto_size_limit(Integer photo_size_limit) {
		this.photo_size_limit = photo_size_limit;
	}
	public com.digitalml.rest.resources.codegentest.Sizes getPhoto_sizes() {
		return photo_sizes;
	}
	
	public void setPhoto_sizes(com.digitalml.rest.resources.codegentest.Sizes photo_sizes) {
		this.photo_sizes = photo_sizes;
	}
}