package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Cursor_lists:
{
  "type": "object",
  "properties": {
    "previous_cursor": {
      "type": "integer",
      "format": "int32"
    },
    "lists": {
      "type": "array",
      "items": {
        "$ref": "Lists"
      }
    },
    "previous_cursor_str": {
      "type": "string"
    },
    "next_cursor": {
      "type": "integer",
      "format": "int32"
    },
    "next_cursor_str": {
      "type": "string"
    }
  }
}
*/

public class Cursor_lists {

	@Size(max=1)
	private Integer previous_cursor;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Lists> lists;

	@Size(max=1)
	private String previous_cursor_str;

	@Size(max=1)
	private Integer next_cursor;

	@Size(max=1)
	private String next_cursor_str;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    lists = new ArrayList<com.digitalml.rest.resources.codegentest.Lists>();
	    previous_cursor_str = null;
	    
	    next_cursor_str = null;
	}
	public Integer getPrevious_cursor() {
		return previous_cursor;
	}
	
	public void setPrevious_cursor(Integer previous_cursor) {
		this.previous_cursor = previous_cursor;
	}
	public List<com.digitalml.rest.resources.codegentest.Lists> getLists() {
		return lists;
	}
	
	public void setLists(List<com.digitalml.rest.resources.codegentest.Lists> lists) {
		this.lists = lists;
	}
	public String getPrevious_cursor_str() {
		return previous_cursor_str;
	}
	
	public void setPrevious_cursor_str(String previous_cursor_str) {
		this.previous_cursor_str = previous_cursor_str;
	}
	public Integer getNext_cursor() {
		return next_cursor;
	}
	
	public void setNext_cursor(Integer next_cursor) {
		this.next_cursor = next_cursor;
	}
	public String getNext_cursor_str() {
		return next_cursor_str;
	}
	
	public void setNext_cursor_str(String next_cursor_str) {
		this.next_cursor_str = next_cursor_str;
	}
}