package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Settings:
{
  "type": "object",
  "properties": {
    "sleep_time": {
      "$ref": "Sleep"
    },
    "use_cookie_personalization": {
      "type": "boolean"
    },
    "trend_location": {
      "type": "array",
      "items": {
        "$ref": "Location"
      }
    },
    "language": {
      "type": "string"
    },
    "discoverable_by_email": {
      "type": "boolean"
    },
    "always_use_https": {
      "type": "boolean"
    },
    "protected": {
      "type": "boolean"
    },
    "geo_enabled": {
      "type": "boolean"
    },
    "show_all_inline_media": {
      "type": "boolean"
    },
    "screen_name": {
      "type": "string"
    }
  }
}
*/

public class Settings {

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Sleep sleep_time;

	@Size(max=1)
	private boolean use_cookie_personalization;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Location> trend_location;

	@Size(max=1)
	private String language;

	@Size(max=1)
	private boolean discoverable_by_email;

	@Size(max=1)
	private boolean always_use_https;

	@Size(max=1)
	private boolean _protected;

	@Size(max=1)
	private boolean geo_enabled;

	@Size(max=1)
	private boolean show_all_inline_media;

	@Size(max=1)
	private String screen_name;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    sleep = new com.digitalml.rest.resources.codegentest.Sleep();
	    
	    trend_location = new ArrayList<com.digitalml.rest.resources.codegentest.Location>();
	    language = null;
	    
	    
	    
	    
	    
	    screen_name = null;
	}
	public com.digitalml.rest.resources.codegentest.Sleep getSleep_time() {
		return sleep_time;
	}
	
	public void setSleep_time(com.digitalml.rest.resources.codegentest.Sleep sleep_time) {
		this.sleep_time = sleep_time;
	}
	public boolean getUse_cookie_personalization() {
		return use_cookie_personalization;
	}
	
	public void setUse_cookie_personalization(boolean use_cookie_personalization) {
		this.use_cookie_personalization = use_cookie_personalization;
	}
	public List<com.digitalml.rest.resources.codegentest.Location> getTrend_location() {
		return trend_location;
	}
	
	public void setTrend_location(List<com.digitalml.rest.resources.codegentest.Location> trend_location) {
		this.trend_location = trend_location;
	}
	public String getLanguage() {
		return language;
	}
	
	public void setLanguage(String language) {
		this.language = language;
	}
	public boolean getDiscoverable_by_email() {
		return discoverable_by_email;
	}
	
	public void setDiscoverable_by_email(boolean discoverable_by_email) {
		this.discoverable_by_email = discoverable_by_email;
	}
	public boolean getAlways_use_https() {
		return always_use_https;
	}
	
	public void setAlways_use_https(boolean always_use_https) {
		this.always_use_https = always_use_https;
	}
	public boolean get_protected() {
		return _protected;
	}
	
	public void set_protected(boolean _protected) {
		this._protected = _protected;
	}
	public boolean getGeo_enabled() {
		return geo_enabled;
	}
	
	public void setGeo_enabled(boolean geo_enabled) {
		this.geo_enabled = geo_enabled;
	}
	public boolean getShow_all_inline_media() {
		return show_all_inline_media;
	}
	
	public void setShow_all_inline_media(boolean show_all_inline_media) {
		this.show_all_inline_media = show_all_inline_media;
	}
	public String getScreen_name() {
		return screen_name;
	}
	
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
}