package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service;
	
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersDestroyByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersDestroyByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersDestroyByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id1ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id1ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoReverse_geoncodeByLatReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoReverse_geoncodeByLatReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoReverse_geoncodeByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSaved_searchesListReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSaved_searchesListReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSaved_searchesListInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpPrivacyReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpPrivacyReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpPrivacyInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_nameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_nameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpConfigurationReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpConfigurationReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpConfigurationInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersShowByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersShowByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersShowByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesShowByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesShowByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesShowByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoPlacesByLatReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoPlacesByLatReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoPlacesByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name1ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name1ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesSentBySince_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesSentBySince_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesSentBySince_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersShowByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersShowByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersShowByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateDeviceReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateDeviceReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateDeviceInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesOembedByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesOembedByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesOembedByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoIdByPlace_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoIdByPlace_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoIdByPlace_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersShowByScreen_nameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersShowByScreen_nameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersShowByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name2ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name2ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name2InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesBySince_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesBySince_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesBySince_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSaved_searchesShowByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSaved_searchesShowByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSaved_searchesShowByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsShowByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsShowByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsShowByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name3ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name3ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name3InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateSkip_statusReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateSkip_statusReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateSkip_statusInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpLanguagesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpLanguagesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpLanguagesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersContributorsByScreen_nameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersContributorsByScreen_nameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersContributorsByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsAvailableReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsAvailableReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsAvailableInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksIdsByStringify_idsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksIdsByStringify_idsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksIdsByStringify_idsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSimilar_placesByLatReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSimilar_placesByLatReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSimilar_placesByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsOutgoingByCursorReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsOutgoingByCursorReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsOutgoingByCursorInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveApplicationRate_limit_statusByResourcesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveApplicationRate_limit_statusByResourcesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveApplicationRate_limit_statusByResourcesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscribersByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersDestroy_allByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersDestroy_allByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersDestroy_allByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersCreate_allByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersCreate_allByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersCreate_allByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsIncomingByCursorReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsIncomingByCursorReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsIncomingByCursorInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveAccountSettingsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveAccountSettingsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveAccountSettingsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTrend_location_woeidReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTrend_location_woeidReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTrend_location_woeidInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsClosestByLatReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsClosestByLatReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsClosestByLatInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersContributeesByScreen_nameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersContributeesByScreen_nameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersContributeesByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsListByScreen_nameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsListByScreen_nameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsListByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTileReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTileReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateTileInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpTosReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpTosReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveHelpTosInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateQueryReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateQueryReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateQueryInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesRetweetsByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesRetweetsByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesRetweetsByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId1ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId1ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId2ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId2ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId2InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksListByInclude_entitiesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksListByInclude_entitiesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveBlocksListByInclude_entitiesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateStatusReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateStatusReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateStatusInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsShowBySource_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsShowBySource_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendshipsShowBySource_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsStatusesByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsStatusesByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsStatusesByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListMembersByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFollowersIdsByUser_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFollowersIdsByUser_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFollowersIdsByUser_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateProfile_background_colorReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateProfile_background_colorReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateProfile_background_colorInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesUser_timelineByCountReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesUser_timelineByCountReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesUser_timelineByCountInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name4ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name4ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name4InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersLookupByScreen_nameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersLookupByScreen_nameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersLookupByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsPlaceByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsPlaceByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveTrendsPlaceByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name5ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name5ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateScreen_name5InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersSearchByQReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersSearchByQReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveUsersSearchByQInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesHome_timelineBySince_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesHome_timelineBySince_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesHome_timelineBySince_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSearchTweetsByQReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSearchTweetsByQReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveSearchTweetsByQInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id2ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id2ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id2InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateUser_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateUser_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateUser_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesShowByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesShowByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveDirect_messagesShowByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesMentions_timelineByCountReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesMentions_timelineByCountReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveStatusesMentions_timelineByCountInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembershipsByUser_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembershipsByUser_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembershipsByUser_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id3ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id3ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateList_id3InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendsIdsByUser_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendsIdsByUser_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveFriendsIdsByUser_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateName1ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateName1ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateName1InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersDestroyByList_idReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersDestroyByList_idReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsMembersDestroyByList_idInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId3ReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId3ReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.CreateId3InputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscriptionsByScreen_nameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscriptionsByScreen_nameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveListsSubscriptionsByScreen_nameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSearchByLatReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSearchByLatReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2Service.RetrieveGeoSearchByLatInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Twitter REST API 2
	 * 120
	 *
	 * @author admin
	 * @version 1.1
	 *
	 */
	
	@Path("https://api.twitter.com/1.1")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class TwitterRESTAPI2Resource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(TwitterRESTAPI2Resource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private TwitterRESTAPI2Service delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.TwitterRESTAPI2.TwitterRESTAPI2ServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private TwitterRESTAPI2Service getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof TwitterRESTAPI2Service)) {
			LOGGER.error(implementationClass + " is not an instance of " + TwitterRESTAPI2Service.class.getName());
			return null;
		}

		return (TwitterRESTAPI2Service)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: retrieveListsSubscribersDestroyByListid
		Returns list of subscribers destroy

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/subscribers/destroy")
	public javax.ws.rs.core.Response retrieveListsSubscribersDestroyByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id) {

		RetrieveListsSubscribersDestroyByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsSubscribersDestroyByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
	
		try {
			RetrieveListsSubscribersDestroyByList_idReturnDTO returnValue = delegateService.retrieveListsSubscribersDestroyByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createListid
		Returns list of members create

	Non-functional requirements:
	*/
	
	@POST
	@Path("/list/members/create")
	public javax.ws.rs.core.Response createListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id) {

		CreateList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setScreen_name(screen_name);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
	
		try {
			CreateList_idReturnDTO returnValue = delegateService.createListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createListid1
		Returns lists of updates

	Non-functional requirements:
	*/
	
	@POST
	@Path("/lists/update")
	public javax.ws.rs.core.Response createListid1(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id,
		@QueryParam("name") String name,
		@QueryParam("mode") String mode,
		@QueryParam("description") String description) {

		CreateList_id1InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateList_id1InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
		inputs.setName(name);
		inputs.setMode(mode);
		inputs.setDescription(description);
	
		try {
			CreateList_id1ReturnDTO returnValue = delegateService.createListid1(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveGeoReversegeoncodeByLat
		Given a latitude and a longitude, searches for up to 20 places that can be used
as a place_id when updatting a status

	Non-functional requirements:
	*/
	
	@GET
	@Path("/geo/reverse_geoncode")
	public javax.ws.rs.core.Response retrieveGeoReversegeoncodeByLat(
		@QueryParam("lat")@NotEmpty String lat,
		@QueryParam("long")@NotEmpty String long,
		@QueryParam("accuracy") String accuracy,
		@QueryParam("granularity") String granularity,
		@QueryParam("max_results") String max_results,
		@QueryParam("callback") String callback) {

		RetrieveGeoReverse_geoncodeByLatInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveGeoReverse_geoncodeByLatInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setLat(lat);
		inputs.set_long(long);
		inputs.setAccuracy(accuracy);
		inputs.setGranularity(granularity);
		inputs.setMax_results(max_results);
		inputs.setCallback(callback);
	
		try {
			RetrieveGeoReverse_geoncodeByLatReturnDTO returnValue = delegateService.retrieveGeoReversegeoncodeByLat(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveSavedsearchesList
		Returns the authenticated user&#x27;s saved search queries

	Non-functional requirements:
	*/
	
	@GET
	@Path("/saved_searches/list")
	public javax.ws.rs.core.Response retrieveSavedsearchesList() {

		RetrieveSaved_searchesListInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveSaved_searchesListInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveSaved_searchesListReturnDTO returnValue = delegateService.retrieveSavedsearchesList(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveHelpPrivacy
		Returns Twitter&#x27;s privacy policy

	Non-functional requirements:
	*/
	
	@GET
	@Path("/help/privacy")
	public javax.ws.rs.core.Response retrieveHelpPrivacy() {

		RetrieveHelpPrivacyInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveHelpPrivacyInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveHelpPrivacyReturnDTO returnValue = delegateService.retrieveHelpPrivacy(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createScreenname
		Returna users report spam

	Non-functional requirements:
	*/
	
	@POST
	@Path("/users/report_spam")
	public javax.ws.rs.core.Response createScreenname(
		@QueryParam("screen_name") String screen_name,
		@QueryParam("user_id") String user_id) {

		CreateScreen_nameInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateScreen_nameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
	
		try {
			CreateScreen_nameReturnDTO returnValue = delegateService.createScreenname(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveHelpConfiguration
		Returns the current configuration used by Twitter including twitter.com slugs
which are not usernames

	Non-functional requirements:
	*/
	
	@GET
	@Path("/help/configuration")
	public javax.ws.rs.core.Response retrieveHelpConfiguration() {

		RetrieveHelpConfigurationInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveHelpConfigurationInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveHelpConfigurationReturnDTO returnValue = delegateService.retrieveHelpConfiguration(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListMembersShowByListid
		Check if the specified user is a member of the specified list

	Non-functional requirements:
	*/
	
	@GET
	@Path("/list/members/show")
	public javax.ws.rs.core.Response retrieveListMembersShowByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		RetrieveListMembersShowByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListMembersShowByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			RetrieveListMembersShowByList_idReturnDTO returnValue = delegateService.retrieveListMembersShowByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveStatusesShowById
		Retruns a single Tweet

	Non-functional requirements:
	*/
	
	@GET
	@Path("/statuses/show/{id}")
	public javax.ws.rs.core.Response retrieveStatusesShowById(
		@PathParam("id")@NotEmpty String id,
		@QueryParam("trim_user") String trim_user,
		@QueryParam("include_my_retweet")@NotEmpty String include_my_retweet,
		@QueryParam("include_entities") String include_entities) {

		RetrieveStatusesShowByIdInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveStatusesShowByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
		inputs.setTrim_user(trim_user);
		inputs.setInclude_my_retweet(include_my_retweet);
		inputs.setInclude_entities(include_entities);
	
		try {
			RetrieveStatusesShowByIdReturnDTO returnValue = delegateService.retrieveStatusesShowById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveGeoPlacesByLat
		Create a new place object at the given latitude and logitude

	Non-functional requirements:
	*/
	
	@GET
	@Path("/geo/places")
	public javax.ws.rs.core.Response retrieveGeoPlacesByLat(
		@QueryParam("lat")@NotEmpty String lat,
		@QueryParam("long")@NotEmpty String long,
		@QueryParam("name")@NotEmpty String name,
		@QueryParam("token")@NotEmpty String token,
		@QueryParam("contained_within") String contained_within,
		@QueryParam("attribute:street_address") String attribute:street_address,
		@QueryParam("callback") String callback) {

		RetrieveGeoPlacesByLatInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveGeoPlacesByLatInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setLat(lat);
		inputs.set_long(long);
		inputs.setName(name);
		inputs.setToken(token);
		inputs.setContained_within(contained_within);
		inputs.setAttributeStreet_address(attribute:street_address);
		inputs.setCallback(callback);
	
		try {
			RetrieveGeoPlacesByLatReturnDTO returnValue = delegateService.retrieveGeoPlacesByLat(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createScreenname1
		blocks the specified user

	Non-functional requirements:
	*/
	
	@POST
	@Path("/blocks/create")
	public javax.ws.rs.core.Response createScreenname1(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		CreateScreen_name1InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateScreen_name1InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			CreateScreen_name1ReturnDTO returnValue = delegateService.createScreenname1(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveDirectmessagesSentBySinceid
		return 20 most recent direct messages sent

	Non-functional requirements:
	*/
	
	@GET
	@Path("/direct_messages/sent")
	public javax.ws.rs.core.Response retrieveDirectmessagesSentBySinceid(
		@QueryParam("since_id") String since_id,
		@QueryParam("max_id") String max_id,
		@QueryParam("count") String count,
		@QueryParam("page") String page,
		@QueryParam("include_entities") String include_entities) {

		RetrieveDirect_messagesSentBySince_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveDirect_messagesSentBySince_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setSince_id(since_id);
		inputs.setMax_id(max_id);
		inputs.setCount(count);
		inputs.setPage(page);
		inputs.setInclude_entities(include_entities);
	
		try {
			RetrieveDirect_messagesSentBySince_idReturnDTO returnValue = delegateService.retrieveDirectmessagesSentBySinceid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsSubscribersShowByListid
		Check if the specified user is a subscriber of the specified list

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/subscribers/show")
	public javax.ws.rs.core.Response retrieveListsSubscribersShowByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("owner_id") String owner_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		RetrieveListsSubscribersShowByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsSubscribersShowByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setOwner_id(owner_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			RetrieveListsSubscribersShowByList_idReturnDTO returnValue = delegateService.retrieveListsSubscribersShowByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createDevice
		sets which device Twitter delivers updates to for user

	Non-functional requirements:
	*/
	
	@POST
	@Path("/account/update_delivery_device")
	public javax.ws.rs.core.Response createDevice(
		@PathParam("device")@NotEmpty String device,
		@QueryParam("include_entities") String include_entities) {

		CreateDeviceInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateDeviceInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setDevice(device);
		inputs.setInclude_entities(include_entities);
	
		try {
			CreateDeviceReturnDTO returnValue = delegateService.createDevice(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveStatusesOembedById
		Returns information allowing the creation of an embedded representation

	Non-functional requirements:
	*/
	
	@GET
	@Path("/statuses/oembed")
	public javax.ws.rs.core.Response retrieveStatusesOembedById(
		@QueryParam("id")@NotEmpty String id,
		@QueryParam("url")@NotEmpty String url,
		@QueryParam("maxwidth") String maxwidth,
		@QueryParam("hide_media") String hide_media,
		@QueryParam("hide_thread") String hide_thread,
		@QueryParam("align") String align,
		@QueryParam("related") String related,
		@QueryParam("lang") String lang) {

		RetrieveStatusesOembedByIdInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveStatusesOembedByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
		inputs.setUrl(url);
		inputs.setMaxwidth(maxwidth);
		inputs.setHide_media(hide_media);
		inputs.setHide_thread(hide_thread);
		inputs.setAlign(align);
		inputs.setRelated(related);
		inputs.setLang(lang);
	
		try {
			RetrieveStatusesOembedByIdReturnDTO returnValue = delegateService.retrieveStatusesOembedById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveGeoIdByPlaceid
		Returns all the information about a know place

	Non-functional requirements:
	*/
	
	@GET
	@Path("/geo/id/{place_id}")
	public javax.ws.rs.core.Response retrieveGeoIdByPlaceid(
		@PathParam("place_id")@NotEmpty String place_id) {

		RetrieveGeoIdByPlace_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveGeoIdByPlace_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setPlace_id(place_id);
	
		try {
			RetrieveGeoIdByPlace_idReturnDTO returnValue = delegateService.retrieveGeoIdByPlaceid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveUsersShowByScreenname
		returns a variety of info about specified user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/users/show")
	public javax.ws.rs.core.Response retrieveUsersShowByScreenname(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id,
		@QueryParam("include_entities") String include_entities) {

		RetrieveUsersShowByScreen_nameInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveUsersShowByScreen_nameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setInclude_entities(include_entities);
	
		try {
			RetrieveUsersShowByScreen_nameReturnDTO returnValue = delegateService.retrieveUsersShowByScreenname(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createScreenname2
		allows user to unfollow user psecified by ID

	Non-functional requirements:
	*/
	
	@POST
	@Path("/friendships/destroy")
	public javax.ws.rs.core.Response createScreenname2(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id) {

		CreateScreen_name2InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateScreen_name2InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
	
		try {
			CreateScreen_name2ReturnDTO returnValue = delegateService.createScreenname2(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveDirectmessagesBySinceid
		return 20 most recent direct messages sent to user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/direct_messages")
	public javax.ws.rs.core.Response retrieveDirectmessagesBySinceid(
		@QueryParam("since_id") String since_id,
		@QueryParam("max_id") String max_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		RetrieveDirect_messagesBySince_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveDirect_messagesBySince_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setSince_id(since_id);
		inputs.setMax_id(max_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			RetrieveDirect_messagesBySince_idReturnDTO returnValue = delegateService.retrieveDirectmessagesBySinceid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveSavedsearchesShowById
		Retrieve the information for the saved search represented by the given id

	Non-functional requirements:
	*/
	
	@GET
	@Path("/saved_searches/show/{id}")
	public javax.ws.rs.core.Response retrieveSavedsearchesShowById(
		@PathParam("id")@NotEmpty String id) {

		RetrieveSaved_searchesShowByIdInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveSaved_searchesShowByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			RetrieveSaved_searchesShowByIdReturnDTO returnValue = delegateService.retrieveSavedsearchesShowById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsShowByListid
		Returns list of show

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/show")
	public javax.ws.rs.core.Response retrieveListsShowByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id) {

		RetrieveListsShowByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsShowByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
	
		try {
			RetrieveListsShowByList_idReturnDTO returnValue = delegateService.retrieveListsShowByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createScreenname3
		Allows one to enable or disable settings for specified user

	Non-functional requirements:
	*/
	
	@POST
	@Path("/friendships/update")
	public javax.ws.rs.core.Response createScreenname3(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id,
		@QueryParam("device")@NotEmpty String device,
		@QueryParam("retweets")@NotEmpty String retweets) {

		CreateScreen_name3InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateScreen_name3InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setDevice(device);
		inputs.setRetweets(retweets);
	
		try {
			CreateScreen_name3ReturnDTO returnValue = delegateService.createScreenname3(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createSkipstatus
		updates user&#x27;s profile image

	Non-functional requirements:
	*/
	
	@POST
	@Path("/account/update_profile_image")
	public javax.ws.rs.core.Response createSkipstatus(
		@QueryParam("skip_status") String skip_status,
		@PathParam("image")@NotEmpty String image) {

		CreateSkip_statusInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateSkip_statusInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setSkip_status(skip_status);
		inputs.setImage(image);
	
		try {
			CreateSkip_statusReturnDTO returnValue = delegateService.createSkipstatus(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createId
		Destroys the status specified by the required ID parameter

	Non-functional requirements:
	*/
	
	@POST
	@Path("/statuses/destroy/{id}")
	public javax.ws.rs.core.Response createId(
		@PathParam("id")@NotEmpty String id,
		@QueryParam("trim_user") String trim_user) {

		CreateIdInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
		inputs.setTrim_user(trim_user);
	
		try {
			CreateIdReturnDTO returnValue = delegateService.createId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveHelpLanguages
		Returns the list of languages supported by Twitter along with the language code
supported by Twitter

	Non-functional requirements:
	*/
	
	@GET
	@Path("/help/languages")
	public javax.ws.rs.core.Response retrieveHelpLanguages() {

		RetrieveHelpLanguagesInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveHelpLanguagesInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveHelpLanguagesReturnDTO returnValue = delegateService.retrieveHelpLanguages(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveUsersContributorsByScreenname
		collection of users that can contribute to specified account

	Non-functional requirements:
	*/
	
	@GET
	@Path("/users/contributors")
	public javax.ws.rs.core.Response retrieveUsersContributorsByScreenname(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		RetrieveUsersContributorsByScreen_nameInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveUsersContributorsByScreen_nameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			RetrieveUsersContributorsByScreen_nameReturnDTO returnValue = delegateService.retrieveUsersContributorsByScreenname(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveTrendsAvailable
		Returns the availability

	Non-functional requirements:
	*/
	
	@GET
	@Path("/trends/available")
	public javax.ws.rs.core.Response retrieveTrendsAvailable() {

		RetrieveTrendsAvailableInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveTrendsAvailableInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveTrendsAvailableReturnDTO returnValue = delegateService.retrieveTrendsAvailable(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveBlocksIdsByStringifyids
		returns array of numeric user ids of blocked users

	Non-functional requirements:
	*/
	
	@GET
	@Path("/blocks/ids")
	public javax.ws.rs.core.Response retrieveBlocksIdsByStringifyids(
		@QueryParam("stringify_ids") String stringify_ids,
		@QueryParam("cursor") String cursor) {

		RetrieveBlocksIdsByStringify_idsInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveBlocksIdsByStringify_idsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setStringify_ids(stringify_ids);
		inputs.setCursor(cursor);
	
		try {
			RetrieveBlocksIdsByStringify_idsReturnDTO returnValue = delegateService.retrieveBlocksIdsByStringifyids(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveGeoSimilarplacesByLat
		Locates places near the given coordinates which are similar in name

	Non-functional requirements:
	*/
	
	@GET
	@Path("/geo/similar_places")
	public javax.ws.rs.core.Response retrieveGeoSimilarplacesByLat(
		@QueryParam("lat")@NotEmpty String lat,
		@QueryParam("long")@NotEmpty String long,
		@QueryParam("name")@NotEmpty String name,
		@QueryParam("contained_within") String contained_within,
		@QueryParam("attribute:street_address") String attribute:street_address,
		@QueryParam("callback") String callback) {

		RetrieveGeoSimilar_placesByLatInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveGeoSimilar_placesByLatInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setLat(lat);
		inputs.set_long(long);
		inputs.setName(name);
		inputs.setContained_within(contained_within);
		inputs.setAttributeStreet_address(attribute:street_address);
		inputs.setCallback(callback);
	
		try {
			RetrieveGeoSimilar_placesByLatReturnDTO returnValue = delegateService.retrieveGeoSimilarplacesByLat(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveFriendshipsOutgoingByCursor
		returns collection of IDs of users with pending follow request from the user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/friendships/outgoing")
	public javax.ws.rs.core.Response retrieveFriendshipsOutgoingByCursor(
		@QueryParam("cursor") String cursor,
		@QueryParam("stringify_ids") String stringify_ids) {

		RetrieveFriendshipsOutgoingByCursorInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveFriendshipsOutgoingByCursorInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setCursor(cursor);
		inputs.setStringify_ids(stringify_ids);
	
		try {
			RetrieveFriendshipsOutgoingByCursorReturnDTO returnValue = delegateService.retrieveFriendshipsOutgoingByCursor(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveApplicationRatelimitstatusByResources
		Returns the current rate limits for methods belonging to the specified resource
families

	Non-functional requirements:
	*/
	
	@GET
	@Path("/application/rate_limit_status")
	public javax.ws.rs.core.Response retrieveApplicationRatelimitstatusByResources(
		 List<String> resources) {

		RetrieveApplicationRate_limit_statusByResourcesInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveApplicationRate_limit_statusByResourcesInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setResources(resources);
	
		try {
			RetrieveApplicationRate_limit_statusByResourcesReturnDTO returnValue = delegateService.retrieveApplicationRatelimitstatusByResources(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsSubscribersByListid
		Returns the subscribers of the specified list

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/subscribers")
	public javax.ws.rs.core.Response retrieveListsSubscribersByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id,
		@QueryParam("cursor") String cursor,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		RetrieveListsSubscribersByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsSubscribersByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
		inputs.setCursor(cursor);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			RetrieveListsSubscribersByList_idReturnDTO returnValue = delegateService.retrieveListsSubscribersByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListMembersDestroyallByListid
		Returns lists of destroy all

	Non-functional requirements:
	*/
	
	@GET
	@Path("/list/members/destroy_all")
	public javax.ws.rs.core.Response retrieveListMembersDestroyallByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id) {

		RetrieveListMembersDestroy_allByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListMembersDestroy_allByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
	
		try {
			RetrieveListMembersDestroy_allByList_idReturnDTO returnValue = delegateService.retrieveListMembersDestroyallByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsMembersCreateallByListid
		Returns lists of members create_all

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/members/create_all")
	public javax.ws.rs.core.Response retrieveListsMembersCreateallByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id,
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name) {

		RetrieveListsMembersCreate_allByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsMembersCreate_allByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
	
		try {
			RetrieveListsMembersCreate_allByList_idReturnDTO returnValue = delegateService.retrieveListsMembersCreateallByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveFriendshipsIncomingByCursor
		returns collection of IDs of users with pending follow request

	Non-functional requirements:
	*/
	
	@GET
	@Path("/friendships/incoming")
	public javax.ws.rs.core.Response retrieveFriendshipsIncomingByCursor(
		@QueryParam("cursor") String cursor,
		@QueryParam("stringify_ids") String stringify_ids) {

		RetrieveFriendshipsIncomingByCursorInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveFriendshipsIncomingByCursorInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setCursor(cursor);
		inputs.setStringify_ids(stringify_ids);
	
		try {
			RetrieveFriendshipsIncomingByCursorReturnDTO returnValue = delegateService.retrieveFriendshipsIncomingByCursor(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveAccountSettings
		returns settings for user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/account/settings")
	public javax.ws.rs.core.Response retrieveAccountSettings() {

		RetrieveAccountSettingsInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveAccountSettingsInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveAccountSettingsReturnDTO returnValue = delegateService.retrieveAccountSettings(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createTrendlocationwoeid
		updates user&#x27;s settings

	Non-functional requirements:
	*/
	
	@POST
	@Path("/account/settings")
	public javax.ws.rs.core.Response createTrendlocationwoeid(
		@QueryParam("trend_location_woeid") String trend_location_woeid,
		@QueryParam("sleep_time_enabled") String sleep_time_enabled,
		@QueryParam("start_sleep_time") String start_sleep_time,
		@QueryParam("end_sleep_time") String end_sleep_time,
		@QueryParam("time_zone") String time_zone,
		@QueryParam("lang") String lang) {

		CreateTrend_location_woeidInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateTrend_location_woeidInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setTrend_location_woeid(trend_location_woeid);
		inputs.setSleep_time_enabled(sleep_time_enabled);
		inputs.setStart_sleep_time(start_sleep_time);
		inputs.setEnd_sleep_time(end_sleep_time);
		inputs.setTime_zone(time_zone);
		inputs.setLang(lang);
	
		try {
			CreateTrend_location_woeidReturnDTO returnValue = delegateService.createTrendlocationwoeid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveTrendsClosestByLat
		Returns the location that Twitter has trending topic information for

	Non-functional requirements:
	*/
	
	@GET
	@Path("/trends/closest")
	public javax.ws.rs.core.Response retrieveTrendsClosestByLat(
		@QueryParam("lat")@NotEmpty String lat,
		@QueryParam("long")@NotEmpty String long) {

		RetrieveTrendsClosestByLatInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveTrendsClosestByLatInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setLat(lat);
		inputs.set_long(long);
	
		try {
			RetrieveTrendsClosestByLatReturnDTO returnValue = delegateService.retrieveTrendsClosestByLat(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveUsersContributeesByScreenname
		collection of users specified user can contribute to

	Non-functional requirements:
	*/
	
	@GET
	@Path("/users/contributees")
	public javax.ws.rs.core.Response retrieveUsersContributeesByScreenname(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		RetrieveUsersContributeesByScreen_nameInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveUsersContributeesByScreen_nameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			RetrieveUsersContributeesByScreen_nameReturnDTO returnValue = delegateService.retrieveUsersContributeesByScreenname(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsListByScreenname
		Return all lists the authenticating or specified user subscribes to, including
their own.

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/list")
	public javax.ws.rs.core.Response retrieveListsListByScreenname(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id) {

		RetrieveListsListByScreen_nameInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsListByScreen_nameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
	
		try {
			RetrieveListsListByScreen_nameReturnDTO returnValue = delegateService.retrieveListsListByScreenname(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createTile
		updates user&#x27;s profile background image

	Non-functional requirements:
	*/
	
	@POST
	@Path("/account/update_profile_background_image")
	public javax.ws.rs.core.Response createTile(
		@QueryParam("tile") String tile,
		@QueryParam("use") String use,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status,
		@PathParam("file")@NotEmpty String file) {

		CreateTileInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateTileInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setTile(tile);
		inputs.setUse(use);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
		inputs.setFile(file);
	
		try {
			CreateTileReturnDTO returnValue = delegateService.createTile(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createName
		sets values that users ar eable to set under Account tab

	Non-functional requirements:
	*/
	
	@POST
	@Path("/account/update_profile")
	public javax.ws.rs.core.Response createName(
		@QueryParam("name") String name,
		@QueryParam("url") String url,
		@QueryParam("location") String location,
		@QueryParam("description") String description,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		CreateNameInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setName(name);
		inputs.setUrl(url);
		inputs.setLocation(location);
		inputs.setDescription(description);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			CreateNameReturnDTO returnValue = delegateService.createName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveHelpTos
		Returns the Twitter Terms of Service

	Non-functional requirements:
	*/
	
	@GET
	@Path("/help/tos")
	public javax.ws.rs.core.Response retrieveHelpTos() {

		RetrieveHelpTosInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveHelpTosInputParametersDTO();
		// Prepare and check all input parameters

	
		try {
			RetrieveHelpTosReturnDTO returnValue = delegateService.retrieveHelpTos(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createQuery
		Create a new saved search for the authenticated user

	Non-functional requirements:
	*/
	
	@POST
	@Path("/saved_searches/create")
	public javax.ws.rs.core.Response createQuery(
		@QueryParam("query")@NotEmpty String query) {

		CreateQueryInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateQueryInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setQuery(query);
	
		try {
			CreateQueryReturnDTO returnValue = delegateService.createQuery(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveStatusesRetweetsById
		Retruns a collection of the 100 most recent retweets of the tweet specified by
the id

	Non-functional requirements:
	*/
	
	@GET
	@Path("/statuses/retweets/{id}")
	public javax.ws.rs.core.Response retrieveStatusesRetweetsById(
		@PathParam("id")@NotEmpty String id,
		@QueryParam("count") String count,
		@QueryParam("trim_user") String trim_user) {

		RetrieveStatusesRetweetsByIdInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveStatusesRetweetsByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
		inputs.setCount(count);
		inputs.setTrim_user(trim_user);
	
		try {
			RetrieveStatusesRetweetsByIdReturnDTO returnValue = delegateService.retrieveStatusesRetweetsById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createId1
		Retweens a tweet

	Non-functional requirements:
	*/
	
	@POST
	@Path("/statuses/retweets/{id}")
	public javax.ws.rs.core.Response createId1(
		@PathParam("id")@NotEmpty String id,
		@QueryParam("trim_user") String trim_user) {

		CreateId1InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateId1InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
		inputs.setTrim_user(trim_user);
	
		try {
			CreateId1ReturnDTO returnValue = delegateService.createId1(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createId2
		destroys direct messages specified in required ID

	Non-functional requirements:
	*/
	
	@POST
	@Path("/direct_messages/destroy")
	public javax.ws.rs.core.Response createId2(
		@QueryParam("id")@NotEmpty String id,
		@QueryParam("include_entities") String include_entities) {

		CreateId2InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateId2InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
		inputs.setInclude_entities(include_entities);
	
		try {
			CreateId2ReturnDTO returnValue = delegateService.createId2(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveBlocksListByIncludeentities
		disallows retweets and device notifications from a user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/blocks/list")
	public javax.ws.rs.core.Response retrieveBlocksListByIncludeentities(
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status,
		@QueryParam("cursor") String cursor) {

		RetrieveBlocksListByInclude_entitiesInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveBlocksListByInclude_entitiesInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
		inputs.setCursor(cursor);
	
		try {
			RetrieveBlocksListByInclude_entitiesReturnDTO returnValue = delegateService.retrieveBlocksListByIncludeentities(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createStatus
		Updates the authenticating user&#x27;s status

	Non-functional requirements:
	*/
	
	@POST
	@Path("/statuses/update")
	public javax.ws.rs.core.Response createStatus(
		@PathParam("status")@NotEmpty String status,
		@QueryParam("in_reply_to_status_id") String in_reply_to_status_id,
		@QueryParam("lat") String lat,
		@QueryParam("long") String long,
		@QueryParam("place_id") String place_id,
		@QueryParam("display_coordinates") String display_coordinates,
		@QueryParam("trim_user") String trim_user) {

		CreateStatusInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateStatusInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setStatus(status);
		inputs.setIn_reply_to_status_id(in_reply_to_status_id);
		inputs.setLat(lat);
		inputs.set_long(long);
		inputs.setPlace_id(place_id);
		inputs.setDisplay_coordinates(display_coordinates);
		inputs.setTrim_user(trim_user);
	
		try {
			CreateStatusReturnDTO returnValue = delegateService.createStatus(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveFriendshipsShowBySourceid
		returns detailed info about relationship between two users

	Non-functional requirements:
	*/
	
	@GET
	@Path("/friendships/show")
	public javax.ws.rs.core.Response retrieveFriendshipsShowBySourceid(
		@QueryParam("source_id") String source_id,
		@QueryParam("source_screen_name") String source_screen_name,
		@QueryParam("target_id")@NotEmpty String target_id,
		@QueryParam("target_screen_name")@NotEmpty String target_screen_name) {

		RetrieveFriendshipsShowBySource_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveFriendshipsShowBySource_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setSource_id(source_id);
		inputs.setSource_screen_name(source_screen_name);
		inputs.setTarget_id(target_id);
		inputs.setTarget_screen_name(target_screen_name);
	
		try {
			RetrieveFriendshipsShowBySource_idReturnDTO returnValue = delegateService.retrieveFriendshipsShowBySourceid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsStatusesByListid
		Returns a timeline of tweets authored by memebers of the specified list

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/statuses")
	public javax.ws.rs.core.Response retrieveListsStatusesByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id,
		@QueryParam("since_id") String since_id,
		@QueryParam("max_id") String max_id,
		@QueryParam("count") String count,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("include_rts") String include_rts) {

		RetrieveListsStatusesByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsStatusesByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
		inputs.setSince_id(since_id);
		inputs.setMax_id(max_id);
		inputs.setCount(count);
		inputs.setInclude_entities(include_entities);
		inputs.setInclude_rts(include_rts);
	
		try {
			RetrieveListsStatusesByList_idReturnDTO returnValue = delegateService.retrieveListsStatusesByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListMembersByListid
		Returns the members of the specified list

	Non-functional requirements:
	*/
	
	@GET
	@Path("/list/members")
	public javax.ws.rs.core.Response retrieveListMembersByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status,
		@QueryParam("cursor") String cursor) {

		RetrieveListMembersByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListMembersByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
		inputs.setCursor(cursor);
	
		try {
			RetrieveListMembersByList_idReturnDTO returnValue = delegateService.retrieveListMembersByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveFollowersIdsByUserid
		returns a cursored collection of user IDs following the user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/followers/ids")
	public javax.ws.rs.core.Response retrieveFollowersIdsByUserid(
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("cursor") String cursor,
		@QueryParam("stringify_ids") String stringify_ids,
		@QueryParam("count") String count) {

		RetrieveFollowersIdsByUser_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveFollowersIdsByUser_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setCursor(cursor);
		inputs.setStringify_ids(stringify_ids);
		inputs.setCount(count);
	
		try {
			RetrieveFollowersIdsByUser_idReturnDTO returnValue = delegateService.retrieveFollowersIdsByUserid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createProfilebackgroundcolor
		sets one or more hex values that controls color scheme

	Non-functional requirements:
	*/
	
	@POST
	@Path("/account/update_profile_colors")
	public javax.ws.rs.core.Response createProfilebackgroundcolor(
		@QueryParam("profile_background_color") String profile_background_color,
		@QueryParam("profile_link_color") String profile_link_color,
		@QueryParam("profile_sidebar_border_color") String profile_sidebar_border_color,
		@QueryParam("profile_sidebar_fill_color") String profile_sidebar_fill_color,
		@QueryParam("profile_text_color") String profile_text_color,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		CreateProfile_background_colorInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateProfile_background_colorInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setProfile_background_color(profile_background_color);
		inputs.setProfile_link_color(profile_link_color);
		inputs.setProfile_sidebar_border_color(profile_sidebar_border_color);
		inputs.setProfile_sidebar_fill_color(profile_sidebar_fill_color);
		inputs.setProfile_text_color(profile_text_color);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			CreateProfile_background_colorReturnDTO returnValue = delegateService.createProfilebackgroundcolor(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveStatusesUsertimelineByCount
		Returns a collection of the most recent Tweets posted by the User

	Non-functional requirements:
	*/
	
	@GET
	@Path("/statuses/user_timeline")
	public javax.ws.rs.core.Response retrieveStatusesUsertimelineByCount(
		@QueryParam("count") String count,
		@QueryParam("since_id") String since_id,
		@QueryParam("max_id") String max_id,
		@QueryParam("trim_user") String trim_user,
		@QueryParam("exclude_replies") boolean exclude_replies,
		@QueryParam("contributor_details") boolean contributor_details,
		@QueryParam("include_rts") boolean include_rts) {

		RetrieveStatusesUser_timelineByCountInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveStatusesUser_timelineByCountInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setCount(count);
		inputs.setSince_id(since_id);
		inputs.setMax_id(max_id);
		inputs.setTrim_user(trim_user);
		inputs.setExclude_replies(exclude_replies);
		inputs.setContributor_details(contributor_details);
		inputs.setInclude_rts(include_rts);
	
		try {
			RetrieveStatusesUser_timelineByCountReturnDTO returnValue = delegateService.retrieveStatusesUsertimelineByCount(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createScreenname4
		allows users to follow user sepcified by ID

	Non-functional requirements:
	*/
	
	@POST
	@Path("/friendships/create")
	public javax.ws.rs.core.Response createScreenname4(
		@QueryParam("screen_name") String screen_name,
		@QueryParam("user_id") String user_id,
		@QueryParam("follow") String follow) {

		CreateScreen_name4InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateScreen_name4InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setFollow(follow);
	
		try {
			CreateScreen_name4ReturnDTO returnValue = delegateService.createScreenname4(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveUsersLookupByScreenname
		returns fully-hydrated user objects up to 100

	Non-functional requirements:
	*/
	
	@GET
	@Path("/users/lookup")
	public javax.ws.rs.core.Response retrieveUsersLookupByScreenname(
		@QueryParam("screen_name") String screen_name,
		@QueryParam("user_id") String user_id,
		@QueryParam("include_entities") String include_entities) {

		RetrieveUsersLookupByScreen_nameInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveUsersLookupByScreen_nameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setInclude_entities(include_entities);
	
		try {
			RetrieveUsersLookupByScreen_nameReturnDTO returnValue = delegateService.retrieveUsersLookupByScreenname(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveTrendsPlaceById
		Returns the top 10 trending topics for a specific WOEID

	Non-functional requirements:
	*/
	
	@GET
	@Path("/trends/place")
	public javax.ws.rs.core.Response retrieveTrendsPlaceById(
		@QueryParam("id")@NotEmpty String id,
		@QueryParam("exclude")@NotEmpty String exclude) {

		RetrieveTrendsPlaceByIdInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveTrendsPlaceByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
		inputs.setExclude(exclude);
	
		try {
			RetrieveTrendsPlaceByIdReturnDTO returnValue = delegateService.retrieveTrendsPlaceById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createScreenname5
		un-blocks the specified user

	Non-functional requirements:
	*/
	
	@POST
	@Path("/blocks/destroy")
	public javax.ws.rs.core.Response createScreenname5(
		@QueryParam("screen_name")@NotEmpty String screen_name,
		@QueryParam("user_id")@NotEmpty String user_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("skip_status") String skip_status) {

		CreateScreen_name5InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateScreen_name5InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setInclude_entities(include_entities);
		inputs.setSkip_status(skip_status);
	
		try {
			CreateScreen_name5ReturnDTO returnValue = delegateService.createScreenname5(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveUsersSearchByQ
		simple relevance-based user search

	Non-functional requirements:
	*/
	
	@GET
	@Path("/users/search")
	public javax.ws.rs.core.Response retrieveUsersSearchByQ(
		@QueryParam("q")@NotEmpty String q,
		@QueryParam("page") String page,
		@QueryParam("count") String count,
		@QueryParam("include_entities") String include_entities) {

		RetrieveUsersSearchByQInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveUsersSearchByQInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setQ(q);
		inputs.setPage(page);
		inputs.setCount(count);
		inputs.setInclude_entities(include_entities);
	
		try {
			RetrieveUsersSearchByQReturnDTO returnValue = delegateService.retrieveUsersSearchByQ(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveStatusesHometimelineBySinceid
		Returns a collection of the most recent Tweets

	Non-functional requirements:
	*/
	
	@GET
	@Path("/statuses/home_timeline")
	public javax.ws.rs.core.Response retrieveStatusesHometimelineBySinceid(
		@QueryParam("since_id") String since_id,
		@QueryParam("max_id") String max_id,
		@QueryParam("trim_user") String trim_user,
		@QueryParam("exclude_replies") boolean exclude_replies,
		@QueryParam("contributor_details") boolean contributor_details) {

		RetrieveStatusesHome_timelineBySince_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveStatusesHome_timelineBySince_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setSince_id(since_id);
		inputs.setMax_id(max_id);
		inputs.setTrim_user(trim_user);
		inputs.setExclude_replies(exclude_replies);
		inputs.setContributor_details(contributor_details);
	
		try {
			RetrieveStatusesHome_timelineBySince_idReturnDTO returnValue = delegateService.retrieveStatusesHometimelineBySinceid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveSearchTweetsByQ
		returns collection of relevant Tweets matching query

	Non-functional requirements:
	*/
	
	@GET
	@Path("/search/tweets")
	public javax.ws.rs.core.Response retrieveSearchTweetsByQ(
		@QueryParam("q")@NotEmpty String q,
		@QueryParam("geocode") String geocode,
		@QueryParam("lang") String lang,
		@QueryParam("locale") String locale,
		@QueryParam("result_type") String result_type,
		@QueryParam("count") String count,
		@QueryParam("until") String until,
		@QueryParam("since_id") String since_id,
		@QueryParam("max_id") String max_id,
		@QueryParam("include_entities") String include_entities,
		@QueryParam("callback") String callback) {

		RetrieveSearchTweetsByQInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveSearchTweetsByQInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setQ(q);
		inputs.setGeocode(geocode);
		inputs.setLang(lang);
		inputs.setLocale(locale);
		inputs.setResult_type(result_type);
		inputs.setCount(count);
		inputs.setUntil(until);
		inputs.setSince_id(since_id);
		inputs.setMax_id(max_id);
		inputs.setInclude_entities(include_entities);
		inputs.setCallback(callback);
	
		try {
			RetrieveSearchTweetsByQReturnDTO returnValue = delegateService.retrieveSearchTweetsByQ(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createListid2
		Subscribes the authenticated user to the specified list

	Non-functional requirements:
	*/
	
	@POST
	@Path("/lists/subscribers/create")
	public javax.ws.rs.core.Response createListid2(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id) {

		CreateList_id2InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateList_id2InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
	
		try {
			CreateList_id2ReturnDTO returnValue = delegateService.createListid2(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createUserid
		sends a new direct message to specified user

	Non-functional requirements:
	*/
	
	@POST
	@Path("/direct_messages/new")
	public javax.ws.rs.core.Response createUserid(
		@QueryParam("user_id")@NotEmpty String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("text")@NotEmpty String text) {

		CreateUser_idInputParametersDTO inputs = new TwitterRESTAPI2Service.CreateUser_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setText(text);
	
		try {
			CreateUser_idReturnDTO returnValue = delegateService.createUserid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveDirectmessagesShowById
		returns a single direct message specified by an id

	Non-functional requirements:
	*/
	
	@GET
	@Path("/direct_messages/show")
	public javax.ws.rs.core.Response retrieveDirectmessagesShowById(
		@QueryParam("id")@NotEmpty String id) {

		RetrieveDirect_messagesShowByIdInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveDirect_messagesShowByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			RetrieveDirect_messagesShowByIdReturnDTO returnValue = delegateService.retrieveDirectmessagesShowById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveStatusesMentionstimelineByCount
		Returns the 20 most recent mentions for the authenticating user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/statuses/mentions_timeline")
	public javax.ws.rs.core.Response retrieveStatusesMentionstimelineByCount(
		@QueryParam("count") String count,
		@QueryParam("since_id") String since_id,
		@QueryParam("max_id") String max_id,
		@QueryParam("trim_user") String trim_user,
		@QueryParam("contributor_details") String contributor_details,
		@QueryParam("include_entities") String include_entities) {

		RetrieveStatusesMentions_timelineByCountInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveStatusesMentions_timelineByCountInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setCount(count);
		inputs.setSince_id(since_id);
		inputs.setMax_id(max_id);
		inputs.setTrim_user(trim_user);
		inputs.setContributor_details(contributor_details);
		inputs.setInclude_entities(include_entities);
	
		try {
			RetrieveStatusesMentions_timelineByCountReturnDTO returnValue = delegateService.retrieveStatusesMentionstimelineByCount(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsMembershipsByUserid
		Returns the lists of the specified user has been added to

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/memberships")
	public javax.ws.rs.core.Response retrieveListsMembershipsByUserid(
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("cursor") String cursor,
		@QueryParam("filter_to_owned_lists") String filter_to_owned_lists) {

		RetrieveListsMembershipsByUser_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsMembershipsByUser_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setCursor(cursor);
		inputs.setFilter_to_owned_lists(filter_to_owned_lists);
	
		try {
			RetrieveListsMembershipsByUser_idReturnDTO returnValue = delegateService.retrieveListsMembershipsByUserid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createListid3
		Returns list of destroy

	Non-functional requirements:
	*/
	
	@POST
	@Path("/lists/destroy")
	public javax.ws.rs.core.Response createListid3(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("owner_id") String owner_id) {

		CreateList_id3InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateList_id3InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setOwner_id(owner_id);
	
		try {
			CreateList_id3ReturnDTO returnValue = delegateService.createListid3(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveFriendsIdsByUserid
		returns a cursored collection of user IDs followed by user

	Non-functional requirements:
	*/
	
	@GET
	@Path("/friends/ids")
	public javax.ws.rs.core.Response retrieveFriendsIdsByUserid(
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("cursor") String cursor,
		@QueryParam("stringify_ids") String stringify_ids,
		@QueryParam("count") String count) {

		RetrieveFriendsIdsByUser_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveFriendsIdsByUser_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setCursor(cursor);
		inputs.setStringify_ids(stringify_ids);
		inputs.setCount(count);
	
		try {
			RetrieveFriendsIdsByUser_idReturnDTO returnValue = delegateService.retrieveFriendsIdsByUserid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createName1
		Returns list of create

	Non-functional requirements:
	*/
	
	@POST
	@Path("/lists/create")
	public javax.ws.rs.core.Response createName1(
		@QueryParam("name") String name,
		@QueryParam("mode") String mode,
		@QueryParam("description") String description) {

		CreateName1InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateName1InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setName(name);
		inputs.setMode(mode);
		inputs.setDescription(description);
	
		try {
			CreateName1ReturnDTO returnValue = delegateService.createName1(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsMembersDestroyByListid
		Returns the list of memebers destroy

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/members/destroy")
	public javax.ws.rs.core.Response retrieveListsMembersDestroyByListid(
		@QueryParam("list_id")@NotEmpty String list_id,
		@QueryParam("slug")@NotEmpty String slug,
		@QueryParam("owner_screen_name") String owner_screen_name,
		@QueryParam("user_id") String user_id,
		@QueryParam("screen_name") String screen_name,
		@QueryParam("owner_id") String owner_id) {

		RetrieveListsMembersDestroyByList_idInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsMembersDestroyByList_idInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setList_id(list_id);
		inputs.setSlug(slug);
		inputs.setOwner_screen_name(owner_screen_name);
		inputs.setUser_id(user_id);
		inputs.setScreen_name(screen_name);
		inputs.setOwner_id(owner_id);
	
		try {
			RetrieveListsMembersDestroyByList_idReturnDTO returnValue = delegateService.retrieveListsMembersDestroyByListid(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createId3
		Destroy a saved search for the authenticating user

	Non-functional requirements:
	*/
	
	@POST
	@Path("/saved_searches/destroy/{id}")
	public javax.ws.rs.core.Response createId3(
		@PathParam("id")@NotEmpty String id) {

		CreateId3InputParametersDTO inputs = new TwitterRESTAPI2Service.CreateId3InputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			CreateId3ReturnDTO returnValue = delegateService.createId3(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveListsSubscriptionsByScreenname
		Returns list of subscriptions

	Non-functional requirements:
	*/
	
	@GET
	@Path("/lists/subscriptions")
	public javax.ws.rs.core.Response retrieveListsSubscriptionsByScreenname(
		@QueryParam("screen_name") String screen_name,
		@QueryParam("user_id") String user_id,
		@QueryParam("count") String count,
		@QueryParam("cursor") String cursor) {

		RetrieveListsSubscriptionsByScreen_nameInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveListsSubscriptionsByScreen_nameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setScreen_name(screen_name);
		inputs.setUser_id(user_id);
		inputs.setCount(count);
		inputs.setCursor(cursor);
	
		try {
			RetrieveListsSubscriptionsByScreen_nameReturnDTO returnValue = delegateService.retrieveListsSubscriptionsByScreenname(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: retrieveGeoSearchByLat
		Search for places that can be attached to a statuses/updates

	Non-functional requirements:
	*/
	
	@GET
	@Path("/geo/search")
	public javax.ws.rs.core.Response retrieveGeoSearchByLat(
		@QueryParam("lat")@NotEmpty String lat,
		@QueryParam("long")@NotEmpty String long,
		@QueryParam("query")@NotEmpty String query,
		@QueryParam("ip")@NotEmpty String ip,
		@QueryParam("accuracy") String accuracy,
		@QueryParam("granularity") String granularity,
		@QueryParam("contained_within") String contained_within,
		@QueryParam("attribute:street_address") String attribute:street_address,
		@QueryParam("callback") String callback) {

		RetrieveGeoSearchByLatInputParametersDTO inputs = new TwitterRESTAPI2Service.RetrieveGeoSearchByLatInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setLat(lat);
		inputs.set_long(long);
		inputs.setQuery(query);
		inputs.setIp(ip);
		inputs.setAccuracy(accuracy);
		inputs.setGranularity(granularity);
		inputs.setContained_within(contained_within);
		inputs.setAttributeStreet_address(attribute:street_address);
		inputs.setCallback(callback);
	
		try {
			RetrieveGeoSearchByLatReturnDTO returnValue = delegateService.retrieveGeoSearchByLat(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}