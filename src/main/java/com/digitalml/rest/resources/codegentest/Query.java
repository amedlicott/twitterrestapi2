package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Query:
{
  "type": "object",
  "properties": {
    "created_at": {
      "type": "string"
    },
    "id": {
      "type": "integer",
      "format": "int32"
    },
    "id_str": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "position": {
      "type": "string"
    },
    "query": {
      "type": "string"
    }
  }
}
*/

public class Query {

	@Size(max=1)
	private String created_at;

	@Size(max=1)
	private Integer id;

	@Size(max=1)
	private String id_str;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String position;

	@Size(max=1)
	private String query;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    created_at = null;
	    
	    id_str = null;
	    name = null;
	    position = null;
	    query = null;
	}
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getId_str() {
		return id_str;
	}
	
	public void setId_str(String id_str) {
		this.id_str = id_str;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	
	public void setPosition(String position) {
		this.position = position;
	}
	public String getQuery() {
		return query;
	}
	
	public void setQuery(String query) {
		this.query = query;
	}
}