package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Places:
{
  "type": "object",
  "properties": {
    "attributes": {
      "type": "string"
    },
    "bounding_box": {
      "$ref": "Bounding_box"
    },
    "country": {
      "type": "string"
    },
    "country_code": {
      "type": "string"
    },
    "full_name": {
      "type": "string"
    },
    "id": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "place_type": {
      "type": "string"
    },
    "url": {
      "type": "string"
    }
  }
}
*/

public class Places {

	@Size(max=1)
	private String attributes;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Bounding_box bounding_box;

	@Size(max=1)
	private String country;

	@Size(max=1)
	private String country_code;

	@Size(max=1)
	private String full_name;

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String place_type;

	@Size(max=1)
	private String url;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    attributes = null;
	    bounding_box = new com.digitalml.rest.resources.codegentest.Bounding.box();
	    country = null;
	    country_code = null;
	    full_name = null;
	    id = null;
	    name = null;
	    place_type = null;
	    url = null;
	}
	public String getAttributes() {
		return attributes;
	}
	
	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}
	public com.digitalml.rest.resources.codegentest.Bounding_box getBounding_box() {
		return bounding_box;
	}
	
	public void setBounding_box(com.digitalml.rest.resources.codegentest.Bounding_box bounding_box) {
		this.bounding_box = bounding_box;
	}
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCountry_code() {
		return country_code;
	}
	
	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
	public String getFull_name() {
		return full_name;
	}
	
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getPlace_type() {
		return place_type;
	}
	
	public void setPlace_type(String place_type) {
		this.place_type = place_type;
	}
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
}