package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Friendship:
{
  "type": "object",
  "properties": {
    "relationship": {
      "$ref": "Targets"
    },
    "source": {
      "$ref": "Source"
    }
  }
}
*/

public class Friendship {

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Targets relationship;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Source source;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    targets = new com.digitalml.rest.resources.codegentest.Targets();
	    source = new com.digitalml.rest.resources.codegentest.Source();
	}
	public com.digitalml.rest.resources.codegentest.Targets getRelationship() {
		return relationship;
	}
	
	public void setRelationship(com.digitalml.rest.resources.codegentest.Targets relationship) {
		this.relationship = relationship;
	}
	public com.digitalml.rest.resources.codegentest.Source getSource() {
		return source;
	}
	
	public void setSource(com.digitalml.rest.resources.codegentest.Source source) {
		this.source = source;
	}
}