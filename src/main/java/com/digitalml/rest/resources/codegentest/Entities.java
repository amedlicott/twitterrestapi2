package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Entities:
{
  "type": "object",
  "properties": {
    "hashtags": {
      "type": "array",
      "items": {
        "$ref": "Hashtags"
      }
    },
    "media": {
      "type": "array",
      "items": {
        "$ref": "Media"
      }
    },
    "urls": {
      "type": "array",
      "items": {
        "$ref": "URL"
      }
    },
    "user_mentions": {
      "type": "array",
      "items": {
        "$ref": "User_Mention"
      }
    }
  }
}
*/

public class Entities {

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Hashtags> hashtags;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Media> media;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.URL> urls;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.User_Mention> user_mentions;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    hashtags = new ArrayList<com.digitalml.rest.resources.codegentest.Hashtags>();
	    media = new ArrayList<com.digitalml.rest.resources.codegentest.Media>();
	    urls = new ArrayList<com.digitalml.rest.resources.codegentest.URL>();
	    user_mentions = new ArrayList<com.digitalml.rest.resources.codegentest.User_Mention>();
	}
	public List<com.digitalml.rest.resources.codegentest.Hashtags> getHashtags() {
		return hashtags;
	}
	
	public void setHashtags(List<com.digitalml.rest.resources.codegentest.Hashtags> hashtags) {
		this.hashtags = hashtags;
	}
	public List<com.digitalml.rest.resources.codegentest.Media> getMedia() {
		return media;
	}
	
	public void setMedia(List<com.digitalml.rest.resources.codegentest.Media> media) {
		this.media = media;
	}
	public List<com.digitalml.rest.resources.codegentest.URL> getUrls() {
		return urls;
	}
	
	public void setUrls(List<com.digitalml.rest.resources.codegentest.URL> urls) {
		this.urls = urls;
	}
	public List<com.digitalml.rest.resources.codegentest.User_Mention> getUser_mentions() {
		return user_mentions;
	}
	
	public void setUser_mentions(List<com.digitalml.rest.resources.codegentest.User_Mention> user_mentions) {
		this.user_mentions = user_mentions;
	}
}