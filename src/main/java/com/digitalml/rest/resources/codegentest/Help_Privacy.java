package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Help_Privacy:
{
  "type": "object",
  "properties": {
    "privacy": {
      "type": "string"
    }
  }
}
*/

public class Help_Privacy {

	@Size(max=1)
	private String privacy;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    privacy = null;
	}
	public String getPrivacy() {
		return privacy;
	}
	
	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}
}