package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Messages:
{
  "type": "object",
  "properties": {
    "created_at": {
      "type": "string"
    },
    "entities": {
      "$ref": "Entities"
    },
    "id": {
      "type": "integer",
      "format": "int32"
    },
    "id_string": {
      "type": "string"
    },
    "recipient": {
      "$ref": "Users"
    },
    "recipient_id": {
      "type": "integer",
      "format": "int32"
    },
    "recipient_screen_name": {
      "type": "string"
    },
    "sender": {
      "$ref": "Users"
    },
    "sender_id": {
      "type": "integer",
      "format": "int32"
    },
    "sender_screen_name": {
      "type": "string"
    },
    "text": {
      "type": "string"
    }
  }
}
*/

public class Messages {

	@Size(max=1)
	private String created_at;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Entities entities;

	@Size(max=1)
	private Integer id;

	@Size(max=1)
	private String id_string;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Users recipient;

	@Size(max=1)
	private Integer recipient_id;

	@Size(max=1)
	private String recipient_screen_name;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Users sender;

	@Size(max=1)
	private Integer sender_id;

	@Size(max=1)
	private String sender_screen_name;

	@Size(max=1)
	private String text;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    created_at = null;
	    entities = new com.digitalml.rest.resources.codegentest.Entities();
	    
	    id_string = null;
	    users = new com.digitalml.rest.resources.codegentest.Users();
	    
	    recipient_screen_name = null;
	    users = new com.digitalml.rest.resources.codegentest.Users();
	    
	    sender_screen_name = null;
	    text = null;
	}
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public com.digitalml.rest.resources.codegentest.Entities getEntities() {
		return entities;
	}
	
	public void setEntities(com.digitalml.rest.resources.codegentest.Entities entities) {
		this.entities = entities;
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getId_string() {
		return id_string;
	}
	
	public void setId_string(String id_string) {
		this.id_string = id_string;
	}
	public com.digitalml.rest.resources.codegentest.Users getRecipient() {
		return recipient;
	}
	
	public void setRecipient(com.digitalml.rest.resources.codegentest.Users recipient) {
		this.recipient = recipient;
	}
	public Integer getRecipient_id() {
		return recipient_id;
	}
	
	public void setRecipient_id(Integer recipient_id) {
		this.recipient_id = recipient_id;
	}
	public String getRecipient_screen_name() {
		return recipient_screen_name;
	}
	
	public void setRecipient_screen_name(String recipient_screen_name) {
		this.recipient_screen_name = recipient_screen_name;
	}
	public com.digitalml.rest.resources.codegentest.Users getSender() {
		return sender;
	}
	
	public void setSender(com.digitalml.rest.resources.codegentest.Users sender) {
		this.sender = sender;
	}
	public Integer getSender_id() {
		return sender_id;
	}
	
	public void setSender_id(Integer sender_id) {
		this.sender_id = sender_id;
	}
	public String getSender_screen_name() {
		return sender_screen_name;
	}
	
	public void setSender_screen_name(String sender_screen_name) {
		this.sender_screen_name = sender_screen_name;
	}
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
}