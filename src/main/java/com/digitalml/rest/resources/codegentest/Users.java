package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Users:
{
  "type": "object",
  "properties": {
    "contributors_enabled": {
      "type": "boolean"
    },
    "created_at": {
      "type": "string"
    },
    "default_profile": {
      "type": "boolean"
    },
    "default_profile_image": {
      "type": "boolean"
    },
    "description": {
      "type": "string"
    },
    "entities": {
      "$ref": "Entities"
    },
    "favorites_count": {
      "type": "integer",
      "format": "int32"
    },
    "follow_request_sent": {
      "type": "boolean"
    },
    "following": {
      "type": "boolean"
    },
    "followers_count": {
      "type": "integer",
      "format": "int32"
    },
    "friends_count": {
      "type": "integer",
      "format": "int32"
    },
    "geo_enabled": {
      "type": "boolean"
    },
    "id": {
      "type": "integer",
      "format": "int32"
    },
    "id_str": {
      "type": "string"
    },
    "is_translator": {
      "type": "boolean"
    },
    "lang": {
      "type": "string"
    },
    "listed_count": {
      "type": "integer",
      "format": "int32"
    },
    "location": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "notifications": {
      "type": "boolean"
    },
    "profile_background_color": {
      "type": "string"
    },
    "profile_background_image_url": {
      "type": "string"
    },
    "profile_background_image_url_https": {
      "type": "string"
    },
    "profile_background_tile": {
      "type": "string"
    },
    "profile_banner_url": {
      "type": "string"
    },
    "profile_image_url": {
      "type": "string"
    },
    "profile_image_url_https": {
      "type": "string"
    },
    "profile_link_color": {
      "type": "string"
    },
    "profile_sidebar_border_color": {
      "type": "string"
    },
    "profile_sidebar_fill_color": {
      "type": "string"
    },
    "profile_text_color": {
      "type": "string"
    },
    "profile_use_background_image": {
      "type": "boolean"
    },
    "protected": {
      "type": "boolean"
    },
    "screen_name": {
      "type": "string"
    },
    "show_all_inline_media": {
      "type": "boolean"
    },
    "status": {
      "$ref": "Tweets"
    },
    "statuses_count": {
      "type": "integer",
      "format": "int32"
    },
    "time_zone": {
      "type": "string"
    },
    "url": {
      "type": "string"
    },
    "utc_offset": {
      "type": "integer",
      "format": "int32"
    },
    "verified": {
      "type": "boolean"
    },
    "withheld_in_countries": {
      "type": "string"
    },
    "withheld_scope": {
      "type": "string"
    }
  }
}
*/

public class Users {

	@Size(max=1)
	private boolean contributors_enabled;

	@Size(max=1)
	private String created_at;

	@Size(max=1)
	private boolean default_profile;

	@Size(max=1)
	private boolean default_profile_image;

	@Size(max=1)
	private String description;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Entities entities;

	@Size(max=1)
	private Integer favorites_count;

	@Size(max=1)
	private boolean follow_request_sent;

	@Size(max=1)
	private boolean following;

	@Size(max=1)
	private Integer followers_count;

	@Size(max=1)
	private Integer friends_count;

	@Size(max=1)
	private boolean geo_enabled;

	@Size(max=1)
	private Integer id;

	@Size(max=1)
	private String id_str;

	@Size(max=1)
	private boolean is_translator;

	@Size(max=1)
	private String lang;

	@Size(max=1)
	private Integer listed_count;

	@Size(max=1)
	private String location;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private boolean notifications;

	@Size(max=1)
	private String profile_background_color;

	@Size(max=1)
	private String profile_background_image_url;

	@Size(max=1)
	private String profile_background_image_url_https;

	@Size(max=1)
	private String profile_background_tile;

	@Size(max=1)
	private String profile_banner_url;

	@Size(max=1)
	private String profile_image_url;

	@Size(max=1)
	private String profile_image_url_https;

	@Size(max=1)
	private String profile_link_color;

	@Size(max=1)
	private String profile_sidebar_border_color;

	@Size(max=1)
	private String profile_sidebar_fill_color;

	@Size(max=1)
	private String profile_text_color;

	@Size(max=1)
	private boolean profile_use_background_image;

	@Size(max=1)
	private boolean _protected;

	@Size(max=1)
	private String screen_name;

	@Size(max=1)
	private boolean show_all_inline_media;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Tweets status;

	@Size(max=1)
	private Integer statuses_count;

	@Size(max=1)
	private String time_zone;

	@Size(max=1)
	private String url;

	@Size(max=1)
	private Integer utc_offset;

	@Size(max=1)
	private boolean verified;

	@Size(max=1)
	private String withheld_in_countries;

	@Size(max=1)
	private String withheld_scope;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    created_at = null;
	    
	    
	    description = null;
	    entities = new com.digitalml.rest.resources.codegentest.Entities();
	    
	    
	    
	    
	    
	    
	    
	    id_str = null;
	    
	    lang = null;
	    
	    location = null;
	    name = null;
	    
	    profile_background_color = null;
	    profile_background_image_url = null;
	    profile_background_image_url_https = null;
	    profile_background_tile = null;
	    profile_banner_url = null;
	    profile_image_url = null;
	    profile_image_url_https = null;
	    profile_link_color = null;
	    profile_sidebar_border_color = null;
	    profile_sidebar_fill_color = null;
	    profile_text_color = null;
	    
	    
	    screen_name = null;
	    
	    tweets = new com.digitalml.rest.resources.codegentest.Tweets();
	    
	    time_zone = null;
	    url = null;
	    
	    
	    withheld_in_countries = null;
	    withheld_scope = null;
	}
	public boolean getContributors_enabled() {
		return contributors_enabled;
	}
	
	public void setContributors_enabled(boolean contributors_enabled) {
		this.contributors_enabled = contributors_enabled;
	}
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public boolean getDefault_profile() {
		return default_profile;
	}
	
	public void setDefault_profile(boolean default_profile) {
		this.default_profile = default_profile;
	}
	public boolean getDefault_profile_image() {
		return default_profile_image;
	}
	
	public void setDefault_profile_image(boolean default_profile_image) {
		this.default_profile_image = default_profile_image;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public com.digitalml.rest.resources.codegentest.Entities getEntities() {
		return entities;
	}
	
	public void setEntities(com.digitalml.rest.resources.codegentest.Entities entities) {
		this.entities = entities;
	}
	public Integer getFavorites_count() {
		return favorites_count;
	}
	
	public void setFavorites_count(Integer favorites_count) {
		this.favorites_count = favorites_count;
	}
	public boolean getFollow_request_sent() {
		return follow_request_sent;
	}
	
	public void setFollow_request_sent(boolean follow_request_sent) {
		this.follow_request_sent = follow_request_sent;
	}
	public boolean getFollowing() {
		return following;
	}
	
	public void setFollowing(boolean following) {
		this.following = following;
	}
	public Integer getFollowers_count() {
		return followers_count;
	}
	
	public void setFollowers_count(Integer followers_count) {
		this.followers_count = followers_count;
	}
	public Integer getFriends_count() {
		return friends_count;
	}
	
	public void setFriends_count(Integer friends_count) {
		this.friends_count = friends_count;
	}
	public boolean getGeo_enabled() {
		return geo_enabled;
	}
	
	public void setGeo_enabled(boolean geo_enabled) {
		this.geo_enabled = geo_enabled;
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getId_str() {
		return id_str;
	}
	
	public void setId_str(String id_str) {
		this.id_str = id_str;
	}
	public boolean getIs_translator() {
		return is_translator;
	}
	
	public void setIs_translator(boolean is_translator) {
		this.is_translator = is_translator;
	}
	public String getLang() {
		return lang;
	}
	
	public void setLang(String lang) {
		this.lang = lang;
	}
	public Integer getListed_count() {
		return listed_count;
	}
	
	public void setListed_count(Integer listed_count) {
		this.listed_count = listed_count;
	}
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public boolean getNotifications() {
		return notifications;
	}
	
	public void setNotifications(boolean notifications) {
		this.notifications = notifications;
	}
	public String getProfile_background_color() {
		return profile_background_color;
	}
	
	public void setProfile_background_color(String profile_background_color) {
		this.profile_background_color = profile_background_color;
	}
	public String getProfile_background_image_url() {
		return profile_background_image_url;
	}
	
	public void setProfile_background_image_url(String profile_background_image_url) {
		this.profile_background_image_url = profile_background_image_url;
	}
	public String getProfile_background_image_url_https() {
		return profile_background_image_url_https;
	}
	
	public void setProfile_background_image_url_https(String profile_background_image_url_https) {
		this.profile_background_image_url_https = profile_background_image_url_https;
	}
	public String getProfile_background_tile() {
		return profile_background_tile;
	}
	
	public void setProfile_background_tile(String profile_background_tile) {
		this.profile_background_tile = profile_background_tile;
	}
	public String getProfile_banner_url() {
		return profile_banner_url;
	}
	
	public void setProfile_banner_url(String profile_banner_url) {
		this.profile_banner_url = profile_banner_url;
	}
	public String getProfile_image_url() {
		return profile_image_url;
	}
	
	public void setProfile_image_url(String profile_image_url) {
		this.profile_image_url = profile_image_url;
	}
	public String getProfile_image_url_https() {
		return profile_image_url_https;
	}
	
	public void setProfile_image_url_https(String profile_image_url_https) {
		this.profile_image_url_https = profile_image_url_https;
	}
	public String getProfile_link_color() {
		return profile_link_color;
	}
	
	public void setProfile_link_color(String profile_link_color) {
		this.profile_link_color = profile_link_color;
	}
	public String getProfile_sidebar_border_color() {
		return profile_sidebar_border_color;
	}
	
	public void setProfile_sidebar_border_color(String profile_sidebar_border_color) {
		this.profile_sidebar_border_color = profile_sidebar_border_color;
	}
	public String getProfile_sidebar_fill_color() {
		return profile_sidebar_fill_color;
	}
	
	public void setProfile_sidebar_fill_color(String profile_sidebar_fill_color) {
		this.profile_sidebar_fill_color = profile_sidebar_fill_color;
	}
	public String getProfile_text_color() {
		return profile_text_color;
	}
	
	public void setProfile_text_color(String profile_text_color) {
		this.profile_text_color = profile_text_color;
	}
	public boolean getProfile_use_background_image() {
		return profile_use_background_image;
	}
	
	public void setProfile_use_background_image(boolean profile_use_background_image) {
		this.profile_use_background_image = profile_use_background_image;
	}
	public boolean get_protected() {
		return _protected;
	}
	
	public void set_protected(boolean _protected) {
		this._protected = _protected;
	}
	public String getScreen_name() {
		return screen_name;
	}
	
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	public boolean getShow_all_inline_media() {
		return show_all_inline_media;
	}
	
	public void setShow_all_inline_media(boolean show_all_inline_media) {
		this.show_all_inline_media = show_all_inline_media;
	}
	public com.digitalml.rest.resources.codegentest.Tweets getStatus() {
		return status;
	}
	
	public void setStatus(com.digitalml.rest.resources.codegentest.Tweets status) {
		this.status = status;
	}
	public Integer getStatuses_count() {
		return statuses_count;
	}
	
	public void setStatuses_count(Integer statuses_count) {
		this.statuses_count = statuses_count;
	}
	public String getTime_zone() {
		return time_zone;
	}
	
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getUtc_offset() {
		return utc_offset;
	}
	
	public void setUtc_offset(Integer utc_offset) {
		this.utc_offset = utc_offset;
	}
	public boolean getVerified() {
		return verified;
	}
	
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public String getWithheld_in_countries() {
		return withheld_in_countries;
	}
	
	public void setWithheld_in_countries(String withheld_in_countries) {
		this.withheld_in_countries = withheld_in_countries;
	}
	public String getWithheld_scope() {
		return withheld_scope;
	}
	
	public void setWithheld_scope(String withheld_scope) {
		this.withheld_scope = withheld_scope;
	}
}