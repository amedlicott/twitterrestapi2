package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Trends:
{
  "type": "object",
  "properties": {
    "events": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "promoted_content": {
      "type": "string"
    },
    "query": {
      "type": "string"
    },
    "url": {
      "type": "string"
    }
  }
}
*/

public class Trends {

	@Size(max=1)
	private String events;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String promoted_content;

	@Size(max=1)
	private String query;

	@Size(max=1)
	private String url;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    events = null;
	    name = null;
	    promoted_content = null;
	    query = null;
	    url = null;
	}
	public String getEvents() {
		return events;
	}
	
	public void setEvents(String events) {
		this.events = events;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getPromoted_content() {
		return promoted_content;
	}
	
	public void setPromoted_content(String promoted_content) {
		this.promoted_content = promoted_content;
	}
	public String getQuery() {
		return query;
	}
	
	public void setQuery(String query) {
		this.query = query;
	}
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
}