package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Contributors:
{
  "type": "object",
  "properties": {
    "id": {
      "type": "integer",
      "format": "int32"
    },
    "id_str": {
      "type": "string"
    },
    "screen_name": {
      "type": "string"
    }
  }
}
*/

public class Contributors {

	@Size(max=1)
	private Integer id;

	@Size(max=1)
	private String id_str;

	@Size(max=1)
	private String screen_name;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    id_str = null;
	    screen_name = null;
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public String getId_str() {
		return id_str;
	}
	
	public void setId_str(String id_str) {
		this.id_str = id_str;
	}
	public String getScreen_name() {
		return screen_name;
	}
	
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
}