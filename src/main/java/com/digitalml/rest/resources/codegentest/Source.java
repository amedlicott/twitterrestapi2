package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Source:
{
  "type": "object",
  "properties": {
    "can_dm": {
      "type": "boolean"
    },
    "blocking": {
      "type": "boolean"
    },
    "id_str": {
      "type": "boolean"
    },
    "all_replies": {
      "type": "boolean"
    },
    "want_retweets": {
      "type": "boolean"
    },
    "id": {
      "type": "integer",
      "format": "int32"
    },
    "marked_spam": {
      "type": "boolean"
    },
    "followed_by": {
      "type": "boolean"
    },
    "notifications_enable": {
      "type": "boolean"
    },
    "screen_name": {
      "type": "string"
    },
    "following": {
      "type": "boolean"
    }
  }
}
*/

public class Source {

	@Size(max=1)
	private boolean can_dm;

	@Size(max=1)
	private boolean blocking;

	@Size(max=1)
	private boolean id_str;

	@Size(max=1)
	private boolean all_replies;

	@Size(max=1)
	private boolean want_retweets;

	@Size(max=1)
	private Integer id;

	@Size(max=1)
	private boolean marked_spam;

	@Size(max=1)
	private boolean followed_by;

	@Size(max=1)
	private boolean notifications_enable;

	@Size(max=1)
	private String screen_name;

	@Size(max=1)
	private boolean following;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    screen_name = null;
	    
	}
	public boolean getCan_dm() {
		return can_dm;
	}
	
	public void setCan_dm(boolean can_dm) {
		this.can_dm = can_dm;
	}
	public boolean getBlocking() {
		return blocking;
	}
	
	public void setBlocking(boolean blocking) {
		this.blocking = blocking;
	}
	public boolean getId_str() {
		return id_str;
	}
	
	public void setId_str(boolean id_str) {
		this.id_str = id_str;
	}
	public boolean getAll_replies() {
		return all_replies;
	}
	
	public void setAll_replies(boolean all_replies) {
		this.all_replies = all_replies;
	}
	public boolean getWant_retweets() {
		return want_retweets;
	}
	
	public void setWant_retweets(boolean want_retweets) {
		this.want_retweets = want_retweets;
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public boolean getMarked_spam() {
		return marked_spam;
	}
	
	public void setMarked_spam(boolean marked_spam) {
		this.marked_spam = marked_spam;
	}
	public boolean getFollowed_by() {
		return followed_by;
	}
	
	public void setFollowed_by(boolean followed_by) {
		this.followed_by = followed_by;
	}
	public boolean getNotifications_enable() {
		return notifications_enable;
	}
	
	public void setNotifications_enable(boolean notifications_enable) {
		this.notifications_enable = notifications_enable;
	}
	public String getScreen_name() {
		return screen_name;
	}
	
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	public boolean getFollowing() {
		return following;
	}
	
	public void setFollowing(boolean following) {
		this.following = following;
	}
}