package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Coordinates:
{
  "type": "object",
  "properties": {
    "coordinates": {
      "type": "array",
      "items": {
        "type": "string"
      }
    },
    "type": {
      "type": "string"
    }
  }
}
*/

public class Coordinates {

	@Size(max=1)
	private List<String> coordinates;

	@Size(max=1)
	private String type;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    coordinates = new ArrayList<String>();
	    type = null;
	}
	public List<String> getCoordinates() {
		return coordinates;
	}
	
	public void setCoordinates(List<String> coordinates) {
		this.coordinates = coordinates;
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
}