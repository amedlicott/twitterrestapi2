package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Sizes:
{
  "type": "object",
  "properties": {
    "thumb": {
      "$ref": "Size"
    },
    "large": {
      "$ref": "Size"
    },
    "medium": {
      "$ref": "Size"
    },
    "small": {
      "$ref": "Size"
    }
  }
}
*/

public class Sizes {

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Size thumb;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Size large;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Size medium;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Size small;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    size = new com.digitalml.rest.resources.codegentest.Size();
	    size = new com.digitalml.rest.resources.codegentest.Size();
	    size = new com.digitalml.rest.resources.codegentest.Size();
	    size = new com.digitalml.rest.resources.codegentest.Size();
	}
	public com.digitalml.rest.resources.codegentest.Size getThumb() {
		return thumb;
	}
	
	public void setThumb(com.digitalml.rest.resources.codegentest.Size thumb) {
		this.thumb = thumb;
	}
	public com.digitalml.rest.resources.codegentest.Size getLarge() {
		return large;
	}
	
	public void setLarge(com.digitalml.rest.resources.codegentest.Size large) {
		this.large = large;
	}
	public com.digitalml.rest.resources.codegentest.Size getMedium() {
		return medium;
	}
	
	public void setMedium(com.digitalml.rest.resources.codegentest.Size medium) {
		this.medium = medium;
	}
	public com.digitalml.rest.resources.codegentest.Size getSmall() {
		return small;
	}
	
	public void setSmall(com.digitalml.rest.resources.codegentest.Size small) {
		this.small = small;
	}
}