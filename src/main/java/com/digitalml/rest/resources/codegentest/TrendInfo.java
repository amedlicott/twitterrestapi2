package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for TrendInfo:
{
  "type": "object",
  "properties": {
    "as_of": {
      "type": "string"
    },
    "created_at": {
      "type": "string"
    },
    "locations": {
      "type": "array",
      "items": {
        "$ref": "Location"
      }
    },
    "trends": {
      "type": "array",
      "items": {
        "$ref": "Trends"
      }
    }
  }
}
*/

public class TrendInfo {

	@Size(max=1)
	private String as_of;

	@Size(max=1)
	private String created_at;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Location> locations;

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.Trends> trends;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    as_of = null;
	    created_at = null;
	    locations = new ArrayList<com.digitalml.rest.resources.codegentest.Location>();
	    trends = new ArrayList<com.digitalml.rest.resources.codegentest.Trends>();
	}
	public String getAs_of() {
		return as_of;
	}
	
	public void setAs_of(String as_of) {
		this.as_of = as_of;
	}
	public String getCreated_at() {
		return created_at;
	}
	
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public List<com.digitalml.rest.resources.codegentest.Location> getLocations() {
		return locations;
	}
	
	public void setLocations(List<com.digitalml.rest.resources.codegentest.Location> locations) {
		this.locations = locations;
	}
	public List<com.digitalml.rest.resources.codegentest.Trends> getTrends() {
		return trends;
	}
	
	public void setTrends(List<com.digitalml.rest.resources.codegentest.Trends> trends) {
		this.trends = trends;
	}
}