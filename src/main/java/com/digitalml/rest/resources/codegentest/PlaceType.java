package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for PlaceType:
{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "code": {
      "type": "integer",
      "format": "int32"
    }
  }
}
*/

public class PlaceType {

	@Size(max=1)
	private String name;

	@Size(max=1)
	private Integer code;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    name = null;
	    
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
}