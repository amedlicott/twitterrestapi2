package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Location:
{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "placeType": {
      "$ref": "PlaceType"
    },
    "woeid": {
      "type": "integer",
      "format": "int32"
    },
    "country": {
      "type": "string"
    },
    "url": {
      "type": "string"
    },
    "countryCode": {
      "type": "string"
    },
    "parentid": {
      "type": "integer",
      "format": "int32"
    }
  }
}
*/

public class Location {

	@Size(max=1)
	private String name;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.PlaceType placeType;

	@Size(max=1)
	private Integer woeid;

	@Size(max=1)
	private String country;

	@Size(max=1)
	private String url;

	@Size(max=1)
	private String countryCode;

	@Size(max=1)
	private Integer parentid;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    name = null;
	    placeType = new com.digitalml.rest.resources.codegentest.PlaceType();
	    
	    country = null;
	    url = null;
	    countryCode = null;
	    
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public com.digitalml.rest.resources.codegentest.PlaceType getPlaceType() {
		return placeType;
	}
	
	public void setPlaceType(com.digitalml.rest.resources.codegentest.PlaceType placeType) {
		this.placeType = placeType;
	}
	public Integer getWoeid() {
		return woeid;
	}
	
	public void setWoeid(Integer woeid) {
		this.woeid = woeid;
	}
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public Integer getParentid() {
		return parentid;
	}
	
	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}
}