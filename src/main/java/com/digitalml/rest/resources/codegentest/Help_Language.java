package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Help_Language:
{
  "type": "object",
  "properties": {
    "code": {
      "type": "string"
    },
    "status": {
      "type": "string"
    },
    "name": {
      "type": "string"
    }
  }
}
*/

public class Help_Language {

	@Size(max=1)
	private String code;

	@Size(max=1)
	private String status;

	@Size(max=1)
	private String name;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    code = null;
	    status = null;
	    name = null;
	}
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}