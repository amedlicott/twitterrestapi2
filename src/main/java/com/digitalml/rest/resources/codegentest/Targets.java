package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Targets:
{
  "type": "object",
  "properties": {
    "target": {
      "$ref": "Target"
    }
  }
}
*/

public class Targets {

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Target target;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    target = new com.digitalml.rest.resources.codegentest.Target();
	}
	public com.digitalml.rest.resources.codegentest.Target getTarget() {
		return target;
	}
	
	public void setTarget(com.digitalml.rest.resources.codegentest.Target target) {
		this.target = target;
	}
}