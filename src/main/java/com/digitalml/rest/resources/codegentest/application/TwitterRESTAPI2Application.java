package com.digitalml.rest.resources.codegentest.application;

import com.digitalml.rest.resources.codegentest.resource.TwitterRESTAPI2Resource;
import com.digitalml.rest.resources.codegentest.resource.ResourceLoggingFilter;					
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Configuration;

import java.util.EnumSet;
import javax.servlet.DispatcherType;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.joran.spi.JoranException;


public class TwitterRESTAPI2Application extends Application<TwitterRESTAPI2Configuration> {

	public static void main(final String[] args) throws Exception {
		new TwitterRESTAPI2Application().run(args);
	}
	
	@Override
	public void initialize(final Bootstrap<TwitterRESTAPI2Configuration> bootstrap) {
		// TODO: application initialization
	}

	@Override
	public void run(final TwitterRESTAPI2Configuration configuration, final Environment environment) {

		//separate logging setup to enable use of external logback xml
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		context.reset();
		ContextInitializer initializer = new ContextInitializer(context);
		try {
			initializer.autoConfig();
		} catch (JoranException e) {
		}

		final TwitterRESTAPI2Resource resource = new TwitterRESTAPI2Resource();
		final TwitterRESTAPI2HealthCheck healthCheck = new TwitterRESTAPI2HealthCheck();
		
		environment.servlets().addFilter("ResourceLoggingFilter", new ResourceLoggingFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
		
		environment.healthChecks().register("template", healthCheck);
		environment.jersey().register(resource);

	}
}

final class TwitterRESTAPI2Configuration extends Configuration {
}

final class TwitterRESTAPI2HealthCheck extends HealthCheck {

	public TwitterRESTAPI2HealthCheck() {
	}

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}
}